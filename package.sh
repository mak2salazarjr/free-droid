#!/bin/bash

#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

cd $(dirname $0)

if ! [ -x "$(command -v jruby)" ]; then
  echo 'Error: jruby command is not available.' >&2
  exit 1
fi

# save build date for versioning
if [ -f res/version.rb ]; then
  previous=$(head -n 1 res/version.rb | awk -F"=" '{print $2}')
  tmp="${previous%\"}"
  tmp="${tmp#\"}"
  previous=$tmp
else
  previous=0
fi
printf "Set new version [previous: $previous]: " && read version
if [ "$version" == "" ]; then version=$previous; fi
echo "VERSION=\"$version\"" > res/version.rb
echo "BUILD=\"$(date +'%Y%m%d')\"" >> res/version.rb

if [[ ! -d pkg ]]; then mkdir pkg; fi
if [[ ! -d pkg/bin ]]; then mkdir pkg/bin; fi

# package to jar
shoes package --jar ./gui.rb

cd pkg

zip -r freedroid.jar LICENSE > /dev/null  # add the LICENSE file to the jar
cp freedroid.jar bin/Free-Droid.jar > /dev/null
mv freedroid.jar Free-Droid-$version.jar > /dev/null

# clean up
rm -rf Free-Droid
if [[ -f Free-Droid-WIN-$version.zip ]]; then rm Free-Droid-WIN-$version.zip; fi
if [[ -f Free-Droid-LINUX-$version.tar.gz ]]; then rm Free-Droid-LINUX-$version.tar.gz; fi
if [[ -f Free-Droid-MAC-$version.tar.gz ]]; then rm Free-Droid-MAC-$version.tar.gz; fi


##### Windows #####
echo "Creating Windows package..."
# prepare zipping
mkdir Free-Droid
cp Free-Droid.exe Free-Droid/Free-Droid.exe
cp -R bin Free-Droid/
cp -R JREs/jre-win-x64 Free-Droid/bin/jre
cp LICENSE Free-Droid/bin/LICENSE
# package the binaries
cp binaries-windows/* Free-Droid/bin/

# compress the Win executable together with JRE and jar-application
zip -r Free-Droid-WIN-$version.zip Free-Droid > /dev/null

# clean up
rm -rf Free-Droid

# create new symlink for latest version
unlink Free-Droid-WIN-latest.zip > /dev/null
ln -s Free-Droid-WIN-$version.zip Free-Droid-WIN-latest.zip


##### Linux #####
echo "Creating Linux package..."
# prepare tarball
mkdir Free-Droid
cp Free-Droid.run Free-Droid/Free-Droid.run
cp -R bin Free-Droid/
cp -R JREs/jre-linux Free-Droid/bin/jre
cp LICENSE Free-Droid/bin/LICENSE

# compress the Linux executable together with JRE and jar-application
tar -pzcvf Free-Droid-LINUX-$version.tar.gz Free-Droid/ > /dev/null

# clean up
rm -rf Free-Droid

# create new symlink for latest version
unlink Free-Droid-LINUX-latest.tar.gz > /dev/null
ln -s Free-Droid-LINUX-$version.tar.gz Free-Droid-LINUX-latest.tar.gz


##### Mac #####
echo "Creating Mac package..."
# prepare zipping
mkdir Free-Droid
cp -R Free-Droid.app Free-Droid/Free-Droid.app
cp -R bin Free-Droid/
cp -R JREs/jre-mac Free-Droid/bin/jre
cp LICENSE Free-Droid/bin/LICENSE
cp "right-click open" Free-Droid/

# compress the Linux executable together with JRE and jar-application
tar -pzcvf Free-Droid-MAC-$version.tar.gz Free-Droid/ > /dev/null

# clean up
rm -rf Free-Droid

# create new symlink for latest version
unlink Free-Droid-MAC-latest.tar.gz > /dev/null
ln -s Free-Droid-MAC-$version.tar.gz Free-Droid-MAC-latest.tar.gz


##### Java #####

# create new symlink for latest version of the jar file
unlink Free-Droid.jar > /dev/null
ln -s Free-Droid-$version.jar Free-Droid.jar

###############


# create checksum files
md5sum Free-Droid-$version.jar > Free-Droid-$version.jar.md5
sha256sum Free-Droid-$version.jar > Free-Droid-$version.jar.sha256

# upload to server
printf "Upload to server? [Y/n]: " && read upload
printf "Trigger update for clients? [Y/n]: " && read update_manifest
if [ ! "$upload" == "n" ]; then
  if [ ! "$update_manifest" == "n" ]; then
    echo "# Version manifest" > update.yml
    echo "---" >> update.yml
    echo "version: $version" >> update.yml
    rsync -l --progress Free-Droid-WIN-$version.zip Free-Droid-WIN-latest.zip Free-Droid-MAC-$version.tar.gz Free-Droid-MAC-latest.tar.gz Free-Droid-LINUX-$version.tar.gz Free-Droid-LINUX-latest.tar.gz Free-Droid-$version.jar Free-Droid-$version.jar.md5 Free-Droid-$version.jar.sha256 Free-Droid.jar update.yml amo@free-droid.com:/srv/update.free-droid.com/
  else
    rsync -l --progress Free-Droid-WIN-$version.zip Free-Droid-WIN-latest.zip Free-Droid-MAC-$version.tar.gz Free-Droid-MAC-latest.tar.gz Free-Droid-LINUX-$version.tar.gz Free-Droid-LINUX-latest.tar.gz Free-Droid-$version.jar Free-Droid-$version.jar.md5 Free-Droid-$version.jar.sha256 Free-Droid.jar amo@free-droid.com:/srv/update.free-droid.com/
  fi
else
  if [ ! "$update_manifest" == "n" ]; then
    echo "# Version manifest" > update.yml
    echo "---" >> update.yml
    echo "version: $version" >> update.yml
    rsync -l --progress update.yml amo@free-droid.com:/srv/update.free-droid.com/
  fi
fi
rm update.yml > /dev/null

# push updates jar and packages to git
printf "Push to git? [Y/n]: " && read pushtogit
if [ ! "$pushtogit" == "n" ]; then
  cd ..
  if [[ ! -d update ]]; then mkdir update; fi
  rm -r update/*
  echo "# Version manifest" > update/update.yml
  echo "---" >> update/update.yml
  echo "version: $version" >> update/update.yml
  cp pkg/*$version* update/
  mv update/Free-Droid-LINUX-$version.tar.gz update/Free-Droid-LINUX.tar.gz
  mv update/Free-Droid-MAC-$version.tar.gz update/Free-Droid-MAC.tar.gz
  mv update/Free-Droid-WIN-$version.zip update/Free-Droid-WIN.zip
  git add update/*
  git add res/version.rb
  git commit -m "Release $version"
  git push
fi
