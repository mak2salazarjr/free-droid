#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# used to prevent calling undefined or unneeded stuff in other modules/files
$cli = true

require_relative "res/log"
extend Log
require_relative "res/manager"
require_relative "res/find"
require_relative "res/get"
load "res/version.rb"

Dir.mkdir("log") unless Dir.exist?("log")
$manager = Manager.new

require "pry"
binding.pry
puts
