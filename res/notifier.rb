#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

class Notifier

  include Log

  def initialize
    @last_states = [nil]
    @last_device = nil
    @updating_twrp = false
    @last_twrp_version = nil
    @twrp_update_successful = nil
  end

  attr_reader :last_states, :last_device

  def returned_from_sideload?
    @last_states[-2] == "sideload" ? true : false
  end

  # called whenever the connection state to the phone changes
  # "device = $device" is nonsense because it creates a copy of the $device object
  # which does not follow with the changes made to $device...
  def update(device = $device, connection_state = nil)
    Main.reset if $aborting
    connection_state = $device.state if connection_state.nil?
    log "Device connection update: #{connection_state ? connection_state : "disconnected"}"
    @last_states << connection_state
    @last_device = $device.codename if $device.codename
    if $device.scan[:codename] != @last_device
      detected_new_device($device)
    else
      detected_same_device($device)
    end if !Main.flashing? && connection_state && !["unauthorized", "sideload", "heimdall", "fastboot"].include?(connection_state)
    case connection_state
    when "booting" then connected_booting($device)
    when "android" then connected_android($device)
    when "heimdall" then connected_heimdall($device)
    when "fastboot" then connected_fastboot($device)
    when "recovery" then connected_recovery($device)
    when "sideload" then connected_sideload($device)
    when "unauthorized" then connected_unauthorized($device)
    else disconnected($device)
    end
    return nil if @updating_twrp
    update_gui($device, connection_state) unless Main.flashing? # update gui from other methods
    if connection_state && Main.flashing? # && connection_state != "sideload"
      Main.flash_conductor(connection_state) # if self.returned_from_sideload?
    end
  end

  def add_device_info
    "Device: #{Find.unique_name_from_codename_csv($device.codename || "")}\nModel: #{$device.name}\nCodename: #{$device.codename}"
  end

  def update_gui(device = $device, connection_state = nil)
    connection_state = $device.state unless connection_state
    if connection_state == "unauthorized"
      Gui.do :update_instructions, "Device unauthorized!\n\nPlease ALLOW and hit OK\non your device screen."
      $ready_to_flash = false
      Gui.do :btn_start_update
    elsif connection_state == "booting"
      # if $manager.something_missing?
      #   $ready_to_flash = false
      #   Gui.do :btn_start_update
      #   Gui.do :update_instructions, "Device connected.\n\nSorry, no Rom available\nfor your device."
      # else
        $ready_to_flash = false
        Gui.do :btn_start_update
        Gui.do :update_instructions, "Device booting..."
      # end
    elsif connection_state
      if $device.codename == "unknown"
        $ready_to_flash = false
        Gui.do :btn_start_update
        Gui.do :update_instructions, "Device unknown, sorry.\n\n#{add_device_info}"
      elsif ["heimdall", "fastboot"].include?(connection_state)
        $ready_to_flash = false
        Gui.do :btn_start_update
        Gui.do :update_instructions, "Device connected.\n\nPlease reboot your device."
      elsif $manager.something_missing?
        $ready_to_flash = false
        Gui.do :btn_start_update
        Gui.do :update_instructions, "Device connected, but no Rom\navailable for your device.\n\n#{add_device_info}"
      else
        # finally, check if the device is supported by Free-Droid
        indicate_device_support
        if $device.supported?
          $ready_to_flash = true
          Gui.do(:btn_start_update)
          Gui.do :update_instructions, "Device connected. Ready.\n\n#{add_device_info}"
        elsif $device.supported?.nil? # untested device
          $ready_to_flash = true
          Gui.do(:btn_start_update)
          Gui.do :update_instructions, "Device connected. Ready.\n\n#{add_device_info}"
        else
          $ready_to_flash = false
          Gui.do(:btn_start_update)
          Gui.do :update_instructions, "Device not supported, sorry.\n\n#{add_device_info}"
        end
      end
    else
      Gui.do :update_instructions, "No device detected\n\nPlease connect your device,\nenable USB debugging and\nOEM unlock (if you have that)."
      $ready_to_flash = false
      Gui.do :btn_start_update
    end
  end

  def detected_new_device(device = $device)
    $manager.reset
    reset_rom_choice_gui
    Gui.do :first_connection
    Gui.do :update_instructions, "Device connected. Loading...\n\n#{add_device_info}"
    Gui.do :update_progress_title, "Searching for available roms..."
    Gui.do :change_choose_your_rom_label, "Scanning..."
    running_rom = $device.custom_rom? ? "custom rom" : "stock rom"
    log "new device: #{$device.name} (codename: #{$device.codename}) (running #{running_rom})"
    if !$session["devices"].include?($device.serial) || $device.device_identified_by_user
      $reporter.report action: "detected (running #{running_rom})"
      if $device.serial && !$device.serial.empty?
        $session["devices"] << $device.serial
        Log.session
      end
    end
    if ["", "unknown"].include?($device.codename)
      candidates = Find.possible_candidates(device.name)
      unless candidates.empty?
        # ask user to identify the device from Find.possible_candidates(device.name)
        log "Asking user to identify the device."
        Find.possible_candidates(device.name)
        Gui.do :identify_device_show
      end
    else
      $manager.scan
      reset_rom_choice_gui($device)
    end
    Gui.do :reset_progressbar
    Gui.do :change_choose_your_rom_label
  end

  def rescan_available_roms
    return false if ["", "unknown", nil].include?($device.codename) || $manager_scanning
    Gui.do :btn_start_deactivate
    $manager.reset
    reset_rom_choice_gui
    Gui.do :change_choose_your_rom_label, "Scanning..."
    $manager.scan
    reset_rom_choice_gui unless ["", "unknown"].include?($device.codename)
    Gui.do :change_choose_your_rom_label
    Gui.do :btn_start_update
  end

  def detected_same_device(device = $device)
    log "same device: #{$device.codename}"
    if $listbox_roms_items.empty? && !["", "unknown"].include?($device.codename) && !Main.flashing?
      $manager.scan
      reset_rom_choice_gui($device)
    end
  end

  def reset_rom_choice_gui(device = $device)
    if $manager.override[:rom]
      Gui.do :chk_archive_tick
      Gui.do :chk_archive_deactivate
      $listbox_roms_items = [$manager.suggest[:rom]]
      Gui.do :update_listbox_roms
      Gui.do :listbox_roms_choose, $manager.suggest[:rom].to_s
    elsif $manager.suggest[:rom_source] == :official
      Gui.do :chk_archive_untick
      $listbox_roms_items = $manager.official.select{|k,v| v}.keys
      Gui.do :update_listbox_roms
      Gui.do :listbox_roms_choose, $manager.suggest[:rom].to_s
      # activate official/archive switch if roms available from other source
      if $manager.archive.values.any?
        Gui.do :chk_archive_activate
      else
        Gui.do :chk_archive_deactivate
      end
    elsif $manager.suggest[:rom_source] == :archive
      Gui.do :chk_archive_tick
      $listbox_roms_items = $manager.archive.select{|k,v| v}.keys
      Gui.do :update_listbox_roms
      Gui.do :listbox_roms_choose, $manager.suggest[:rom].to_s
      # activate official/archive switch if roms available from other source
      if $manager.official.values.any?
        Gui.do :chk_archive_activate
      else
        Gui.do :chk_archive_deactivate
      end
    else # empty the listbox, untick and deactivate archive checkbox
      Gui.do :chk_archive_untick
      Gui.do :chk_archive_deactivate
      $listbox_roms_items = $manager.official.select{|k,v| v}.keys
      Gui.do :update_listbox_roms
      Gui.do :listbox_roms_choose, $manager.suggest[:rom].to_s
    end
    # install magisk by default on non-LineageOS roms # Edit: On all roms now!
    # unless $chk_magisk_clicked
    #   if $manager.suggest[:rom].to_s.downcase.start_with?("lineageos")
    #     Gui.do :chk_magisk_untick
    #   else
    #     Gui.do :chk_magisk_tick unless $manager.suggest[:rom].nil?
    #   end
    # end
  end

  def switch_rom_source
    # determine chosen source
    if $chk_archive_state == false # official sources
      source = :official
    else
      source = :archive
    end
    # update listbox
    if $chk_archive_state == false # official sources
      $listbox_roms_items = $manager.official.select{|k,v| v}.keys
      Gui.do :update_listbox_roms
      if source.to_sym == $manager.suggest[:rom_source]
        Gui.do :listbox_roms_choose, $manager.suggest[:rom].to_s
        $listbox_roms_selected = $manager.suggest[:rom].to_s
      else
        if $listbox_roms_items.map{|e| e.to_sym}.include?(:LineageOS)
          Gui.do :listbox_roms_choose, "LineageOS"
        elsif $listbox_roms_items.map{|e| e.to_sym}.include?(:LineageOS_MicroG)
          Gui.do :listbox_roms_choose, "LineageOS_MicroG"
        else
          Gui.do :listbox_roms_choose, $listbox_roms_items[0].to_s
        end
      end
    else # use archive
      $listbox_roms_items = $manager.archive.select{|k,v| v}.keys
      Gui.do :update_listbox_roms
      if source.to_sym == $manager.suggest[:rom_source]
        Gui.do :listbox_roms_choose, $manager.suggest[:rom].to_s
        $listbox_roms_selected = $manager.suggest[:rom].to_s
      else
        if $listbox_roms_items.map{|e| e.to_sym}.include?(:LineageOS)
          Gui.do :listbox_roms_choose, "LineageOS"
        elsif $listbox_roms_items.map{|e| e.to_sym}.include?(:LineageOS_MicroG)
          Gui.do :listbox_roms_choose, "LineageOS_MicroG"
        else
          Gui.do :listbox_roms_choose, $listbox_roms_items[0].to_s
        end
      end
    end
    tell_manager_about_rom_choice
  end

  # not working because called pre instead of post selection
  # read when hitting start button instead...
  def user_selected_a_rom
    tell_manager_about_rom_choice
  end

  def tell_manager_about_rom_choice(rom = nil)
    return nil if rom.nil? && $listbox_roms_selected.nil?
    rom = $listbox_roms_selected.to_sym if rom.nil?
    if $chk_archive_state == false # official sources
      $manager.user_choice[:rom_source] = :official
    else
      $manager.user_choice[:rom_source] = :archive
    end
    $manager.user_choice[:rom] = rom
    # install magisk by default on non-LineageOS roms # edit: magisk by default.
    # unless $chk_magisk_clicked
    #   if $manager.user_choice[:rom].to_s.downcase.start_with?("lineageos")
    #     Gui.do :chk_magisk_untick
    #   else
    #     Gui.do :chk_magisk_tick
    #   end
    # end
  end

  def indicate_device_support
    if ["", "unknown"].include?($device.codename) || $device.codename.nil?
      return Gui.do(:update_device_state_indicator, "")
    end
    if Find.supported?($device.codename) == true
      Gui.do :update_device_state_indicator, "device supported", 13, "green"
    elsif Find.supported?($device.codename).nil?
      Gui.do :update_device_state_indicator, "device untested", 13, "grey"
    elsif Find.supported?($device.codename) == false
      Gui.do :update_device_state_indicator, "device not supported", 13, "red"
    else
      Gui.do :update_device_state_indicator, Find.supported?($device.codename).to_s, 10, "grey"
    end
  end

  def connected_booting(device)

  end

  def connected_android(device)

  end

  def connected_fastboot(device)

  end

  def connected_heimdall(device)

  end

  def connected_recovery(device)

  end

  def connected_sideload(device)

  end

  def connected_unauthorized(device)

  end

  def disconnected(device)
    unless Main.flashing?
      Gui.do :chk_use_own_rom_untick
    end
  end
end
