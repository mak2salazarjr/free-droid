#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Class for handling logging to standard output and to a log file

require "mechanize" # used for bug report submission

module Log

  # Append lines to the main log file
  def self.add(input)
    File.open("log/free-droid.log", "a") do |file|
      if input.is_a?(Exception)
        Log.add "#{input.class}: #{input.message}"
        Log.add input.backtrace.join("\n")
      elsif input.is_a?(Array)
        input.each {|e| Log.add e.to_s}
      else
        input.lines.each do |line|
          file.puts "#{Time.now} - #{line}\r"
          puts line
        end
      end
    end
  end

  # reads versions and paths of binaries and adds them to context for sentry
  def self.read_versions_of_binaries
    ADB.bin_version # reads $adb_bin_version and $adb_bin_path
    Fastboot.bin_version # reads $fastboot_bin_version and $fastboot_bin_path
    Heimdall.bin_version # reads $heimdall_bin_version
    Raven.tags_context adb_version: $adb_bin_version, adb_path: $adb_bin_path,
      fastboot_version: $fastboot_bin_version, fastboot_path: $fastboot_bin_path,
      heimdall_version: $heimdall_bin_version
  end

  # read the session (or create one)
  def self.read_session
    if File.exist?("log/session")
      $session = YAML.load(open("log/session").read)
    else
      # session (visitor) id must be a 16 characters hexadecimal string for matomo
      $session = {"id" => Log.generate_id, "devices" => []}
    end
  end

  def self.generate_id
    charset = Array('0'..'9') + Array('a'..'f')
    Array.new(16) { charset.sample }.join
  end

  def self.session
    Log.read_session unless $session
    File.write("log/session", $session.to_yaml)
    Raven.user_context id: $session["id"]
    $session
  end

  # Pack all the different log files to a zip
  def self.pack(filename = nil)
    filename = "Free-Droid-#{VERSION}-bugreport-#{Time.now.strftime("%Y-%m-%d")}-#{$session["id"]}.zip" if filename.nil?
    File.delete filename if File.exist? filename
    # logs = Dir.entries("log").select {|e| e.end_with?(".log")}
    logs = Dir[File.join("log/", "**", "**")]
    Zip::File.open(filename, "w") do |zip|
      logs.each { |log| zip.add(log, log) }
    end
    filename
  end

  def self.submit(filename = nil)
    filename = Log.pack if filename.nil?
    webdav_path = "public.php/webdav/files/amo/Free-Droid%20bug%20reports/#{filename}"
    full_url = "https://amaury.cc/#{webdav_path}"
    File.open(filename, "r") do |bugreport|
      agent = Mechanize.new
      agent.add_auth(full_url, 'bugreports', '')
      agent.put(full_url, bugreport.read,
        {'X-Requested-With' => 'XMLHttpRequest', 'Content-Type' => 'application/zip'})
    end
    File.delete filename
  end

  # Shortcut for Log.add (for use as a mixin)
  def log(input)
    Log.add input
  end

  def self.add_context_to_sentry_report
    Log.session
    Log.read_versions_of_binaries
    Raven.tags_context device: $device.codename, device_name: $device.name,
      device_brand: $device.brand, twrp_version: $device.twrp_version,
      os: OS.which, cpu: OS.cpu
  end

  def log_on_fail(options = {quit: true})
    begin
      yield
    rescue Exception => e
      Log.add_context_to_sentry_report
      Raven.capture { raise e } rescue nil if $chk_tracking_consent_state
      log e
      Log.submit if $chk_tracking_consent_state
      if options[:quit]
        if $chk_tracking_consent_state
          Gui.do :close_app, "Sorry, an error occured. A bug report has been sent to the developer."
        else
          Gui.do :close_app, "Sorry, an error occured."
        end
      elsif options[:info]
        if $chk_tracking_consent_state
          Gui.do :alert, "An error occured, sorry about that! A bug report has been sent to the developer."
        else
          Gui.do :alert, "An error occured, sorry about that!"
        end
      end
    end
  end

end
