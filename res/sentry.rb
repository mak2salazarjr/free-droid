#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# For using the Sentry bug tracker

require "sentry-raven"

Raven.configure do |config|
  config.release = VERSION
  config.dsn = 'https://25945441039b4fb899dd8fb979b29355@sentry.io/1874291'
  config.timeout = 20
  config.open_timeout = 20
end

# Raven supports two methods of capturing exceptions:
# Raven.capture do
#   # capture any exceptions which happen during execution of this block
#   1 / 0
# end
#
# begin
#   1 / 0
# rescue ZeroDivisionError => exception
#   Raven.capture_exception(exception)
# end
