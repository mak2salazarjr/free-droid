#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module for downloading the latest Rom of a given distribution
# for a given device. Also verifies the checksums of the Rom.

require "zip"
require "open-uri"
require "net/http"
require "openssl"
require "nokogiri"
require "json"
require_relative "os"
require_relative "find"

# monkey-patch net/http to lower the connection timeouts
module Net
  class HTTP < Protocol
    def initialize(address, port = nil)
      @address = address
      @port    = (port || HTTP.default_port)
      @local_host = nil
      @local_port = nil
      @curr_http_version = HTTPVersion
      @keep_alive_timeout = 2
      @last_communicated = nil
      @close_on_empty_response = false
      @socket  = nil
      @started = false
      @open_timeout = 30
      @read_timeout = 30
      @continue_timeout = nil
      @max_retries = 2
      @debug_output = nil

      @proxy_from_env = false
      @proxy_uri      = nil
      @proxy_address  = nil
      @proxy_port     = nil
      @proxy_user     = nil
      @proxy_pass     = nil

      @use_ssl = false
      @ssl_context = nil
      @ssl_session = nil
      @sspi_enabled = false
      SSL_IVNAMES.each do |ivname|
        instance_variable_set ivname, nil
      end
    end
  end
end

module Get

  extend Log unless $cli # make log() method available as shortcut for Log.add()

  # Hash containing available sources for downloads.
  # Omit trailing / from the URLs.
  # Needed methods to implement for each distribution are:
  #   scrape_distr, distr_available, distr_parse_version and dl_distr
  #   Don't forget to create matches for the distr in rom_twrp_versions_compatible?
  @DISTRIBUTIONS = {
    Platform_Tools: "https://dl.google.com/android/repository",
    # Heimdall: "https://bitbucket.org/benjamin_dobell/heimdall/downloads/?tab=downloads",
    Heimdall: "https://heimdall.free-droid.com",
    TWRP: "https://dl.twrp.me",
    LineageOS: "https://download.lineageos.org",
    # LineageOS_MicroG: "https://download.lineage.microg.org",
    Omnirom: "http://dl.omnirom.org",
    AospExtended: "https://api.aospextended.com/ota",
    ResurrectionRemix: "https://get.resurrectionremix.com",
    CarbonROM: "https://get.carbonrom.org",
    # e: "https://images.ecloud.global/dev",  # name "/e/" might change
    # LineageOS_Root_Addon: "https://archive.free-droid.com/extras",
    Magisk: "https://forum.xda-developers.com/apps/magisk/official-magisk-v7-universal-systemless-t3473445",

    NanoDroid: "https://downloads.nanolx.org/NanoDroid/Stable"
    # NanoDroid_Bromite: "https://downloads.nanolx.org/NanoDroid/Stable",
    # NanoDroid_Patcher: "https://downloads.nanolx.org/NanoDroid/Stable",
    # NanoDroid_Systest: "https://downloads.nanolx.org/NanoDroid/Stable"

    # crDroid: "", # downloads link to sourceforge, androidfilehost, mega, etc...
  }

  @ARCHIVE = "https://archive.free-droid.com"

  # List the distributions from above that are roms
  @ROMS = [:LineageOS, :LineageOS_MicroG, :Omnirom, :crDroid, :ResurrectionRemix,
    :AospExtended, :CarbonROM, :Mockee]

  # Identifying substrings in official filenames of roms
  @ROM_STRINGS_OFFICIAL = ["lineage-", "omni-", "AospExtended-", "crDroidAndroid-",
    "RR-", "CARBON"]

  # Populated by calling the scrape method. Will be a hash with
  # the same keys as @DISTRIBUTIONS. The values will be arrays
  # containing the devices with downloadable release of this distro
  @maintainted_devices = {}

  class << self

    # make @DISTRIBUTIONS readable
    def distributions
      @DISTRIBUTIONS
    end

    # make @ROMS readable
    def roms
      @ROMS
    end

    # make @maintainted_devices readable
    def maintainted_devices
      scrape
      @maintainted_devices
    end

    # Scrapes implemented distritubions for maintained devices
    # Populates @maintainted_devices.
    def scrape  # by distribution
      @DISTRIBUTIONS.each do |key, value|
        @maintainted_devices[key] = send("scrape_#{key}".downcase.to_sym)
      end
      @maintainted_devices
    end

    def devices_with_rom
      result = []
      @DISTRIBUTIONS.each do |key, value|
        if is_a_rom?(key)
          result = result + send("scrape_#{key}".downcase.to_sym)
        end
      end
      result.uniq.sort_by(&:downcase)
    end

    # Scrapes for devices with both TWRP and a Rom available
    def devices_with_twrp_and_rom
      with_twrp = Thread.new{ scrape_twrp }
      with_rom = Thread.new{ devices_with_rom }
      with_twrp.join.value & with_rom.join.value
    end

    # Check if distribution is implemented and their website is reachable,
    #   route to correct download method and give correct URL as argument.
    # Only download if the file is not present yet.
    def get(rom, device = "", timeout_retries = 3)
      retry_on_http_connection_problem do
        device = Find.codename if device.empty? && (is_a_rom?(rom) || rom == :TWRP)
        rom = rom.to_sym
        device = device.downcase.to_sym
        # Check if the requested rom is implemented in here
        if @DISTRIBUTIONS[rom]
          if internet_connection?(@DISTRIBUTIONS[rom])
            # remote_file = available(device)[rom]
            # Checking if file already present has been moved to individual dl_* methods
            # unless File.exist?("#{device}/#{remote_file}") || File.exist?(remote_file)
            #   send("dl_#{rom}".downcase.to_sym, device)
            # else
            #   if File.exist?("#{device}/#{remote_file}")
            #     "#{device}/#{remote_file}"
            #   elsif File.exist?(remote_file)
            #     remote_file
            #   else
            #     false
            #   end
            # end
            send("dl_#{rom}".downcase.to_sym, device)
          else
            log "No connection to the server. Are you connected to the internet?"
          end
        else
          log "Distribution not implemented yet."
        end
      end
    # rescue Net::OpenTimeout, Net::ReadTimeout => e
    #   if timeout_retries <= 1
    #     log "Connection timed out too many times when trying to download #{rom}."
    #     false
    #   else
    #     get(rom, device, timeout_retries - 1)
    #   end
    end

    # Prepare device flashing by downloading all we need
    # Returns an array containing file paths of downloaded  ?!?
    #   TWRP and LineageOS_MicroG files as index 2 and 3.   ?!?
    def all_we_need(device, rom = :LineageOS_MicroG)
      need = [:Platform_Tools, :Heimdall, :TWRP, rom]
      if need.all? { |item| available(device)[item] }
        result = []
        need.each { |item| result << get(item, device) }
        result.all? ? result : false
      else
        not_available = []
        need.each { |item| (not_available << item.to_s unless available(device)[item]) }
        log "Error: No official release of #{not_available[0]} available for your device."
        false
      end
    end

    $ongoing_downloads = []
    @@progress = {total: 0, progress: 0}

    # Chunked download from url to file. Optionally using the progress bar.
    # If file is already present: only redownload if force == true
    # Does not allow downloading twice to the same file at the same time.
    def download(url, file, pbar_args = {}, force = false, max_redirects = 10)
      raise ArgumentError, 'too many HTTP redirects' if max_redirects == 0
      if File.exist?(file)
        return true unless force
      end
      if $ongoing_downloads.include? file
        log caller_locations(1,3) # in case something breaks because of this
        return false
      else
        $ongoing_downloads << file
      end
      begin
        # set default arguments for pbar_args hash
        pbar_args = {progressbar: true, title: file}.merge(pbar_args)
        # parse URI, follow redirects and open connection
        uri = URI url
        retry_on_http_connection_problem do
          Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
            # put together a request, set referer to satisfy twrp site
            http.read_timeout = 30
            request = Net::HTTP::Get.new uri
            request["Referer"] = url #if ["TWRP", "ResurrectionRemix"].include?(pbar_args[:title].to_s.upcase)
            http.request request do |response|
              # follow redirects
              # check out total download size
              if response.is_a? Net::HTTPRedirection
                return download(response["location"], file, pbar_args, force, max_redirects - 1)
              else
                total_size = response.header['content-length'].to_i
                @@progress = {total: 0, progress: 0} if @@progress[:total] <= 0.975 * @@progress[:progress]
                @@progress[:total] += total_size
                # save to file in chunks
                File.open("#{file}.tmp", 'wb') do |io|
                  response.read_body do |chunk|
                    io.write chunk
                    @@progress[:progress] += chunk.length if pbar_args[:progressbar] == true
                    Gui.do :progressbar_set, @@progress[:progress].to_f / @@progress[:total] if pbar_args[:progressbar] == true unless $cli
                  end
                end
                sleep 1
                File.rename "#{file}.tmp", file
              end # if http redirection
            end # http.request
          end # Net::HTTP.start
        end # retry_on_http_connection_problem
      ensure
        $ongoing_downloads = $ongoing_downloads - [file]
      end
    end # def

    # backup of working download method:
    # def download(url, file, pbar_args = {}, force = false)
    #   if File.exist?(file)
    #     return true unless force
    #   end
    #   # set default arguments for pbar_args hash
    #   pbar_args = {progressbar: true, title: file}.merge(pbar_args)
    #   # parse URI and open connection
    #   uri = URI(url)
    #   # uri = URI resolve(url)
    #   Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
    #     # put together a request, set referer to satisfy twrp site
    #     request = Net::HTTP::Get.new uri
    #     request["Referer"] = url if pbar_args[:title].to_s.upcase == "TWRP"
    #     http.request request do |response|
    #       # follow redirects
    #       while response.is_a?(Net::HTTPRedirection)
    #         if response['location'].nil?
    #           new_uri = URI.parse(response.body.match(/<a href=\"([^>]+)\">/i)[1])
    #           response = Net::HTTP.get_response(new_uri)
    #         else
    #           new_uri = URI.parse(response['location'])
    #           response = Net::HTTP.get_response(new_uri)
    #         end
    #       end
    #       # check out total download size
    #       total_size = response.header['content-length'].to_i
    #       # create progress bar unless specified false
    #       if pbar_args[:progressbar] == true
    #         pbar_args[:title] == file ? pbar_title = "Download: #{file}" :
    #           pbar_title = pbar_args[:title].to_s
    #         pbar = ProgressBar.create(title: pbar_title,
    #           total: total_size, :format => "%t |%b>%i| %e")
    #       end
    #       # save to file in chunks
    #       open "#{file}.tmp", 'wb' do |io|
    #         response.read_body do |chunk|
    #           io.write chunk
    #           pbar.progress += chunk.length if pbar_args[:progressbar] == true
    #           Gui.do :progressbar_set, pbar.progress.to_f / total_size if pbar_args[:progressbar] == true
    #         end
    #       end
    #       File.rename "#{file}.tmp", file
    #     end # http.request
    #   end # Net::HTTP.start
    # end # def

    # might help in certain cases (e.g. magisk download)
    # resolve uri (follow redirections) and return uri with http success code
    def resolve(uri_str, max_attempts = 5, timeout = 10, agent = 'curl/7.43.0')
      attempts = 0
      cookie = nil
      until attempts >= max_attempts
        attempts += 1
        url = URI.parse(uri_str)
        http = Net::HTTP.new(url.host, url.port)
        http.open_timeout = timeout
        http.read_timeout = timeout
        path = url.path
        path = '/' if path == ''
        path += '?' + url.query unless url.query.nil?
        params = { 'User-Agent' => agent, 'Accept' => '*/*' }
        params['Cookie'] = cookie unless cookie.nil?
        request = Net::HTTP::Get.new(path, params)
        if url.instance_of?(URI::HTTPS)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        end
        response = http.request(request)
        case response
          when Net::HTTPSuccess then
            break
          when Net::HTTPRedirection then
            location = response['Location']
            cookie = response['Set-Cookie']
            new_uri = URI.parse(location)
            uri_str = if new_uri.relative?
                        url + location
                      else
                        new_uri.to_s
                      end
          else
            raise 'Unexpected response: ' + response.inspect
        end
      end
      raise 'Too many http redirects' if attempts == max_attempts
      uri_str
    end

    # might help in certain cases to redirect exactly once (e.g. LineageOS download)
    def resolve_1(uri_str, timeout = 10, cookie = nil, agent = 'curl/7.43.0')
      url = URI.parse(uri_str)
      http = Net::HTTP.new(url.host, url.port)
      http.open_timeout = timeout
      http.read_timeout = timeout
      path = url.path
      path = '/' if path == ''
      path += '?' + url.query unless url.query.nil?
      params = { 'User-Agent' => agent, 'Accept' => '*/*' }
      params['Cookie'] = cookie unless cookie.nil?
      request = Net::HTTP::Get.new(path, params)
      if url.instance_of?(URI::HTTPS)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      response = http.request(request)
      location = response['Location']
      cookie = response['Set-Cookie']
      new_uri = URI.parse(location)
      uri_str = if new_uri.relative?
                  url + location
                else
                  new_uri.to_s
                end
    end

    def zadig
      url = "https://github.com/pbatard/libwdi/releases/download/b721/zadig-2.4.exe"
      fallback_url = "https://stuff.free-droid.com/zadig-2.4.exe"
      download resolve(url), "bin/zadig.exe", {progressbar: false, title: "zadig"}
      download fallback_url, "bin/zadig.exe", {progressbar: false, title: "zadig"} unless File.exist? "bin/zadig.exe"
      File.exist?("bin/zadig.exe") ? "bin/zadig.exe" : nil
    end

    def adb_driver
      File.delete "universaladbdriver_v6.0.zip" rescue nil
      File.delete "universaladbdriver_v6.0.exe" rescue nil
      url = "https://cdn.universaladbdriver.com/wp-content/uploads/universaladbdriver_v6.0.zip"
      fallback_url = "https://stuff.free-droid.com/universaladbdriver_v6.0.zip"
      zip = "universaladbdriver_v6.0.zip"
      download url, zip, {progressbar: false, title: "adb driver"}
      download fallback_url, zip, {progressbar: false, title: "adb driver"} unless File.exist? zip
      if File.exist? zip
        begin
          retries ||= 0
          Zip::File.open(zip) do |z|
            z.each do |f|
              if f.name == "universaladbdriver_v6.0/universaladbdriver_v6.0.exe"
                f.extract "universaladbdriver_v6.0.exe"
              end
            end
          end
        rescue Zip::Error
          retry if (retries += 1) < 3
        end
      end
      File.delete "universaladbdriver_v6.0.zip" rescue nil
      File.exist?("universaladbdriver_v6.0.exe") ? "universaladbdriver_v6.0.exe" : nil
    end

    # Returns hash with latest available release as value for each key
    # taken from @DISTRIBUTIONS.
    def available(device, what = distributions.keys)
      result = {}
      threads = []
      distributions.each do |key, value|
        next unless what.include? key
        threads << Thread.new {
          # no need for this as net/http already retries (once) on timeout
          # timeout_retries = 1   # set to 1 so we don't retry at this point
          # begin
          #   if internet_connection?(value)
          #     result[key] = send("#{key}_available".downcase.to_sym, device)
          #   else
          #     log "No connection to #{value}"
          #     result[key] = nil
          #   end
          # rescue Net::OpenTimeout, Net::ReadTimeout => e
          #   if timeout_retries <= 1
          #     log "Connection timed out too many times when trying to check availability of #{key}."
          #     false
          #   else
          #     timeout_retries = timeout_retries - 1
          #     retry
          #   end
          # end
          retry_on_http_connection_problem do
            if internet_connection?(value)
              result[key] = send("#{key}_available".downcase.to_sym, device)
            else
              log "No connection to #{value}"
              result[key] = nil
            end
          end
        }
      end
      result[:Archive] = archive_latest_available(device)
      threads.each { |thr| thr.join }
      # Some devices have different codenames for TWRP or are grouped
      # under one codename for TWRP compared to LineageOS
      result.each_key do |distr|
        unless result[distr]
          result[distr] = send("#{distr.downcase}_available".to_sym, Find.alias(device, distr))
          # tell the $manager if we have to use a different codename for TWRP or rom
          if result[distr]
            if distr == :TWRP
              $manager.use_alt_name[:TWRP] = Find.alias(device, distr)
            else
              $manager.use_alt_name[distr] = Find.alias(device, distr)
            end
          end
        end
        if distr == :Archive && result[:Archive].empty?
          result[:Archive] = archive_latest_available(Find.alias(device, distr))
        end
      end
      result
    end

    # Returns latest available versions of @DISTRIBUTIONS for
    # the given device as a hash like @DISTRIBUTIONS.
    # Includes available latest versions from the archive as nested hash.
    def available_versions(device, availables = nil)
      availables = available(device) if availables.nil?
      result = {}
      availables.each do |key, value|
        if value == false || value.empty?
          result[key] = nil
        elsif key == :Archive
          result[key] = available_versions(device, value)
        else
          if respond_to? "#{key}_parse_version".downcase.to_sym
            result[key] = send("#{key}_parse_version".downcase, value)
          else
            result[key] = generic_parse_version(value).downcase
          end
        end
      end
      result
    end

    # Like available_versions but with only the available Roms showing
    def available_rom_versions(device)
      available_versions(device).select do |k,v|
        if k == :Archive
          v && !v.empty?
        else
          is_a_rom?(k) && !v.nil?
        end
      end
    end

    # check if we can find a TWRP image and a rom from official source or archive
    def rom_and_twrp_available?(device)
      available_twrp_rom(device).all?
    end

    def available_twrp_rom(device)
      found = available(device)
      # check for rom availability
      official_rom = found.reject do |k,v|
        [:Heimdall,:Archive,:Platform_Tools,:NanoDroid,:Magisk,:TWRP].include? k
      end.any?{|k,v| v}
      archive_rom = found[:Archive].reject{|k,v| k == :TWRP}.any?{|k,v| v}
      rom = official_rom || archive_rom
      # check for twrp availability
      twrp = found[:TWRP] || found[:Archive][:TWRP]
      [rom, twrp]
    end

    def generic_parse_version(filename)
      filename[/(?:(\d+\.[.\d]*\d+))/]
    end

    # verify file_path against available checksums download url of file
    def verify(file_path, url)
      # file_name = File.basename(URI(url).path)
      return false unless File.exist? file_path
      check_count = 0
      ["md5","md5sum"].each do |suffix|
        if remote_file_exist? "#{url}.#{suffix}"
          md5 = open("#{url}.#{suffix}", &:readline).split(" ")[0].strip
          return false unless md5 == Digest::MD5.file(file_path).to_s.strip
          check_count =+ 1
        end
      end
      ["sha256","sha256sum"].each do |suffix|
        if remote_file_exist? "#{url}.#{suffix}"
          sha256 = open("#{url}.#{suffix}", &:readline).split(" ")[0].strip
          return false unless sha256 == Digest::SHA256.file(file_path).to_s.strip
          check_count =+ 1
        end
      end
      check_count.zero? ? (log "Warning: Could not verify downloaded file: #{file_path}") : true
      true
    end

    # check if given zip file (or symbol) is a rom
    def is_a_rom?(zip_file)
      return @ROMS.include?(zip_file) if zip_file.is_a?(Symbol)
      zip_file = File.basename(zip_file) if zip_file.is_a?(String)
      return false unless zip_file.end_with?(".zip")
      @ROM_STRINGS_OFFICIAL.each do |item|
        return true if zip_file.downcase.include?(item.downcase)
      end
      false
    end

    def which_distribution(zip_file)
      if zip_file.end_with?(".zip")
        zip = zip_file.downcase
        if zip.include? "lineage" then :LineageOS
        elsif zip.start_with? "rr" then :ResurrectionRemix
        elsif zip.include? "resurrection" then :ResurrectionRemix
        elsif zip.include? "omni-" then :Omnirom
        elsif zip.include? "twrp" then :TWRP
        elsif zip.include? "crdroid" then :crDroid
        elsif zip.include? "aospextended" then :AospExtended
        elsif zip.start_with? "mk" then :Mockee
        elsif zip.include? "mockee" then :Mockee
        else false
        end
      elsif zip_file.end_with?(".img")
        img = zip_file.downcase
        img.include?("twrp") ? :TWRP : false
      else
        return false
      end
    end

    def rom_version(zip)
      distro = which_distribution zip
      return nil unless distro
      send("#{distro.downcase}_parse_version".to_sym, zip) rescue nil
    end

    def android_version(zip)
      distro = which_distribution zip
      return nil unless distro && is_a_rom?(distro)
      send("#{distro.downcase}_tell_android_version".to_sym, zip) rescue nil
    end

    def android_name(version)
      return nil unless version.is_a?(String)
      case version.split(".")[0].to_i
      when 0..4 then "KitKat"
      when 5 then "Lolipop"
      when 6 then "Marshmallow"
      when 7 then "Nougat"
      when 8 then "Oreo"
      when 9 then "Pie"
      else "?"
      end
    end

    # Checks compatibility of available twrp and rom versions.
    def rom_twrp_versions_compatible?(device, rom = :LineageOS_MicroG)
      twrp_version = available_versions(device)[:TWRP]
      return false unless twrp_version
      # make rom a symbol in case it has been given as a string
      rom = rom.to_sym
      rom_version = available_versions(device)[rom]
      return false unless rom_version
      check_compatibility(twrp_version, rom, rom_version)
    end

    def check_compatibility(twrp_version, rom, rom_version)
      # convert twrp_version to a comparable Version object
      twrp_version = Gem::Version.new(twrp_version.gsub("-","."))
      # make rom a symbol in case it has been given as a string
      rom = rom.to_sym
      # convert rom_version to a comparable Version object
      rom_version = Gem::Version.new(rom_version.gsub("-","."))
      result = nil
      case rom
      when :LineageOS_MicroG
        result = true if rom_version < Gem::Version.new("12")
        result = twrp_version >= Gem::Version.new("2.8.2") if rom_version >= Gem::Version.new("12")
        result = twrp_version >= Gem::Version.new("2.8.6") if rom_version >= Gem::Version.new("12.1")
        result = twrp_version >= Gem::Version.new("3") if rom_version >= Gem::Version.new("13")
        result = twrp_version >= Gem::Version.new("3.1") if rom_version >= Gem::Version.new("14")
        result = twrp_version >= Gem::Version.new("3.1.1") if rom_version >= Gem::Version.new("14.1")
        result = twrp_version >= Gem::Version.new("3.2.1") if rom_version >= Gem::Version.new("15")
        result = twrp_version >= Gem::Version.new("3.2.1") if rom_version >= Gem::Version.new("15.1")
        result = false if rom_version >= Gem::Version.new("16")

        # case rom_version
        # when Gem::Version.new("12") then twrp_version >= Gem::Version.new("2.8.2")
        # when Gem::Version.new("12.1") then twrp_version >= Gem::Version.new("2.8.6")
        # when Gem::Version.new("13") then twrp_version >= Gem::Version.new("3")
        # when Gem::Version.new("14") then twrp_version >= Gem::Version.new("3.1")
        # when Gem::Version.new("14.1") then twrp_version >= Gem::Version.new("3.1.1")
        # when Gem::Version.new("15") then twrp_version >= Gem::Version.new("3.2.1")
        # when Gem::Version.new("15.1") then twrp_version >= Gem::Version.new("3.2.1")
        # # when Gem::Version.new("16") then twrp_version >= Gem::Version.new("999")
        # else false
        # end # case rom_version
      when :Omnirom
        result = true if rom_version < Gem::Version.new("4.4")
        result = twrp_version >= Gem::Version.new("2.8.2") if rom_version >= Gem::Version.new("4.4")
        result = twrp_version >= Gem::Version.new("2.8.2") if rom_version >= Gem::Version.new("5")
        result = twrp_version >= Gem::Version.new("2.8.6") if rom_version >= Gem::Version.new("5.1")
        result = twrp_version >= Gem::Version.new("3") if rom_version >= Gem::Version.new("6")
        result = twrp_version >= Gem::Version.new("3.1") if rom_version >= Gem::Version.new("7")
        result = twrp_version >= Gem::Version.new("3.1.1") if rom_version >= Gem::Version.new("7.1")
        result = twrp_version >= Gem::Version.new("3.2.1") if rom_version >= Gem::Version.new("8")
        result = twrp_version >= Gem::Version.new("3.2.1") if rom_version >= Gem::Version.new("8.1")
        result = false if rom_version >= Gem::Version.new("9")
      else false
      end # case rom
      result
    end

    # check for internet connection
    def internet_connection?(url = "http://www.google.com/")
      begin
        true if http_code(url)
      rescue
        false
      end
    end

    def http_code(url)
      retry_on_http_connection_problem do
        # Net::HTTP.get_response(URI(url), read_timeout: 20, open_timeout: 20, ssl_timeout: 20).code
        Net::HTTP.get_response(URI(url)).code
      end rescue nil
    end

    # also gives true for a folder (url MUST include trailing slash)
    def remote_file_exist?(url)
      uri = URI(url)
      retry_on_http_connection_problem do
        Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
          http.request_head(uri).is_a?(Net::HTTPOK)
        end
      end
    end

    def different_name(device, rom = :TWRP)
      device = device.to_s
      case device
      when "gprimelte" then return "gprimelte"
      when "gprimeltexx" then return "gprimeltexx"
      when "j5ltedx" then return "j5lte"
      when "j5lteub" then return "j5lte"
      when "j5yltedo" then return "j5lte"
      when "d803" then return "g2"
      when "g2" then return "d803"
      when "axon7" then return "ailsa_ii"
      when "ailsa_ii" then return "axon7"
      when "bardockpro" then return "bardock" if rom == :TWRP
      when "yt_x703l" then return "ytx703l"
      when "ytx703l" then return "yt_x703l"
      when "yt_x703f" then return "ytx703f"
      when "ytx703f" then return "yt_x703f"
      when "xt897" then return "asanti_c"
      when "asanti_c" then return "xt897"
      when "r7plus" then return "r7plusf"
      when "r7plusf" then return "r7plus"
      when "j5x3g" then return "j5xnlte" if rom == :TWRP
      when "j5ltedx" then return "j5lte" if rom == :TWRP
      when "j5lteub" then return "j5lte" if rom == :TWRP
      when "j5yltedo" then return "j5lte" if rom == :TWRP
      when "paella" then return "piccolometal"
      when "piccolometal" then return "paella"
      when "m8d" then return "m8" if rom == :TWRP
      when "lt02ltespr" then return "lt02ltetmo" if rom == :TWRP
      when "n1awifi" then return "lt03wifiue" if rom == :TWRP
      when "kuntao" then return "kuntao_row"
      when "kuntao_row" then return "kuntao"
      when "klteactivexx", "kltechn", "kltechnduo", "klteduos", "kltedv", "kltekdi", "kltekor", "kltektt", "kltelget" then return "klte"
      when "himawl", "himaul" then return "hima" if rom == :TWRP
      when "hiaeul", "hiaeuhl" then return "hiae" if rom == :TWRP
      when "ham" then return "Z1" if rom == :TWRP
      when "h815" then return "g2" if rom == :TWRP
      when "flounder_lte" then return "flounder" if rom == :TWRP
      when "ja3g", "ja3gxx" then return "i9500" if rom == :TWRP
      when "F8331", "F8332" then return "kagura"
      when "ahannah", "rhannah", "hannah_t", "hannah_sprint" then return "hannah"
      end
      try_name1 = device.end_with?("xx") ? device.chop.chop : "#{device}xx"
      return try_name1 # if available(try_name1)[rom]
      # device
    end

    def scrape_platform_tools
      # nothing to scrape, returning boolean instead
      platform_tools_available
    end

    # no need for device parameter here, but keeping for code coherence
    def platform_tools_available(device = "")
      dl_platform_tools(device, dry_run: true)
    end

    def platform_tools_parse_version(filename)
      generic_parse_version(filename)
    end

    # no need for device parameter here, but keeping for code coherence
    def dl_platform_tools(device = "", options = {})
      # decide which OS release to look for
      os = OS.which
      os = "darwin" if OS.mac?

      # put together correct download url
      base_url = @DISTRIBUTIONS[:Platform_Tools]
      url = "#{base_url}/platform-tools-latest-#{os}.zip"
      uri = URI(url)

      # return false if the server is not reachable
      return false unless http_code(url)

      # follow redirection from "latest" to version number in url
      retry_on_http_connection_problem do
        Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https') do |http|
          request = Net::HTTP::Get.new uri
          http.request request do |response|
            uri = response["location"]
          end
        end
      end

      filename = "bin/#{uri.split("/")[-1]}"

      # return latest available build file name in case of a dry run
      if options[:dry_run] == true
        return File.basename(filename)
      end

      download(uri, filename, progressbar: true, title: "Platform Tools") ? filename : false

      # extract adb and fastboot binaries
      success = extract_adb_fastboot_from filename

      File.delete filename

      success ? true : false
    end

    def extract_adb_fastboot_from(zip)
      execute "bin/adb kill-server" rescue nil
      begin
        retries ||= 0
        extracted = 0
        Zip::File.open(zip) do |z|
          z.each do |f|
            case f.name
            when "platform-tools/fastboot"
              File.delete "bin/fastboot" rescue nil
              f.extract "bin/fastboot"
              FileUtils.chmod("+x", "bin/fastboot")
              extracted += 1
            when "platform-tools/fastboot.exe"
              File.delete "bin/fastboot.exe" rescue nil
              f.extract "bin/fastboot.exe"
              extracted += 1
            when "platform-tools/adb"
              File.delete "bin/adb" rescue nil
              f.extract("bin/adb")
              FileUtils.chmod("+x", "bin/adb")
              extracted += 1
            when "platform-tools/adb.exe"
              File.delete "bin/adb.exe" rescue nil
              f.extract("bin/adb.exe")
              extracted += 1
            when "platform-tools/AdbWinApi.dll"
              File.delete "bin/AdbWinApi.dll" rescue nil
              f.extract("bin/AdbWinApi.dll")
              extracted += 1
            when "platform-tools/AdbWinUsbApi.dll"
              File.delete "bin/AdbWinUsbApi.dll" rescue nil
              f.extract("bin/AdbWinUsbApi.dll")
              extracted += 1
            when "platform-tools/libwinpthread-1.dll"
              File.delete "bin/libwinpthread-1.dll" rescue nil
              f.extract("bin/libwinpthread-1.dll")
              extracted += 1
            end
          end
        rescue Zip::Error
          retry if (retries += 1) < 3
        end
      end
      if OS.windows?
        extracted == 5 ? true : false
      else
        extracted == 2 ? true : false
      end
    end

    def scrape_heimdall
      # nothing to scrape, returning boolean instead
      heimdall_available
    end

    # no need for device parameter here, but keeping for code coherence
    def heimdall_available(device = "")
      dl_heimdall(device, dry_run: true)
    end

    def heimdall_parse_version(filename)
      filename
    end

    # no need for device parameter here, but keeping for code coherence
    def dl_heimdall(device = "", options = {})
      # decide which OS release to look for
      os = OS.which
      os = "win" if OS.windows?

      # put together correct download url
      base_url = @DISTRIBUTIONS[:Heimdall]
      url = "#{base_url}/heimdall-#{os}.zip"

      # return false if the server is not reachable
      return false unless retry_on_http_connection_problem do http_code(url) end

      # return latest available build file name in case of a dry run
      return "heimdall-#{os}.zip" if options[:dry_run]

      file_path = "bin/heimdall-#{os}.zip"

      # download the file and set title of the progress bar
      download(url, file_path, progressbar: true, title: "Heimdall")

      check = verify(file_path, url)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        download(url, file_path, progressbar: true, title: "Heimdall")
      end

      # extract the zip
      unzip file_path if check

      File.delete(file_path) if File.exist?(file_path)

      # return file_path if all worked out and false otherwise
      File.exist?("bin/heimdall") || File.exist?("bin/heimdall.exe") ? true : false
    end

    # unzips a zip file and overwrites files if they exist
    def unzip(zip)
      begin
        retries ||= 0
        Zip::File.open(zip) do |z|
          z.each do |f|
            File.delete "bin/#{f.name}" rescue nil
            f.extract "bin/#{f.name}"
            FileUtils.chmod("+x", "bin/#{f.name}") unless OS.windows?
          end
        end
      rescue Zip::Error
        retry if (retries += 1) < 3
      end
    end

    # Scrape TWRP for devices with downloadable releases.
    # Returns an array with those devices.
    def scrape_twrp
      # not implemented yet, returning empty array
      threads = []
      brands_url = "https://twrp.me/Devices"
      return [] if internet_connection?(brands_url) == "404"
      brands_page = Nokogiri::HTML(open(brands_url))
      brands = brands_page.css("p")[1..-3].map{|e| e.text.strip}
      brands.each do |brand|
        brand_url = "#{brands_url}/#{brand}"
        next unless internet_connection?(brand_url)
        threads << Thread.new {
          brand_page = Nokogiri::HTML(open(brand_url))
          brand_page.css("p")[1..-3].map{|e| e.text.strip}.map{|e| e.scan(/\(([^)]+)\)/)[-1][0]}
        }
      end
      values = threads.map{|t| t.join.value}
      result = []
      values.each { |e| result = result + e }
      result.sort_by(&:downcase)
    end

    # Checks whether a TWRP image is available for given device.
    def twrp_available(device)
      dl_twrp(device, dry_run: true)
    end

    def twrp_parse_version(filename)
      filename.split("-")[1]
    end

    # Download latest TWRP image from device URL.
    # Returns relative file path if successful and false otherwise.
    def dl_twrp(device, options = {})
      retry_on_http_connection_problem do
      return false if device == "unknown" || device.empty?
      # "https://dl.twrp.me"
      base_url = @DISTRIBUTIONS[:TWRP]

      # e.g. "https://dl.twrp.me/osprey"
      device_url = "#{base_url}/#{device}"

      return false if http_code(device_url) == "404"

      device_page = Nokogiri::HTML(open(device_url))

      # e.g. ["twrp-3.2.3-0-osprey.img", "twrp-3.2.2-0-osprey.img", ...]
      available = device_page.css("table a").map{|href| href.text}.reject{ |href|
        href.end_with?(".tar") || href.end_with?(".zip")}.sort.reverse

      # return latest available build file name in case of a dry run
      return available[0] if options[:dry_run] == true

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.file?(available[0][1..-1]) && options[:force] != true
        return available[0][1..-1]
      end

      Dir.mkdir(device.to_s) unless Dir.exist?(device.to_s)

      # Extract the most recent Rom and put together a path.
      # These paths are used on the local file system.
      # e.g. "osprey/twrp-3.2.3-0-osprey.img"
      file_path = "#{device}/#{available[0]}"

      # e.g. "https://dl.twrp.me/osprey/twrp-3.2.3-0-osprey.img"
      file_full_href = "#{device_url}/#{available[0]}"

      # download the file
      download(file_full_href, file_path, progressbar: true, title: "TWRP")

      # return file_path if all worked out and false otherwise
      result = verify(file_path, file_full_href) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_twrp(device, retry: true)
      else
        result
      end
      end
    end

    # Scrape LineageOS for devices with downloadable releases.
    # Returns an array with those devices.
    def scrape_lineageos
      retry_on_http_connection_problem do
      page = Nokogiri::HTML(open(@DISTRIBUTIONS[:LineageOS]))
      result = page.css("li a").select{|e| e["href"] &&
        e["href"].start_with?("/")}.map{|e| e["href"][1..-1]} - ["extras"]
      result.sort_by(&:downcase)
      end
    end

    # Checks whether a LineageOS_MicroG rom is available for given device.
    def lineageos_available(device)
      dl_lineageos(device, dry_run: true)
    end

    def lineageos_parse_version(filename)
      filename.split("-")[1]
    end

    def lineageos_tell_android_version(filename)
      rom_v = lineageos_parse_version filename
      maj = rom_v.split(".")[0]
      min = rom_v.split(".")[1..-1].join(".")
      android_v = "#{maj.to_i-7}"
      android_v = "#{android_v}.#{min}" unless min.empty?
    end

    def verify_lineageos(file_path, url)
      retry_on_http_connection_problem do
      # file_name = File.basename(URI(url).path)
      return false unless File.exist? file_path
      if remote_file_exist? "#{url}?sha256"
        sha256 = open("#{url}?sha256", &:readline).split(" ")[0].strip
        return false unless sha256 == Digest::SHA256.file(file_path).to_s.strip
      else
        log "Warning: Could not verify downloaded file."
      end
      true
      end
    end

    # Download latest LineageOS Rom from device URL.
    # Returns relative file path if successful and false otherwise.
    def dl_lineageos(device, options = {})
      retry_on_http_connection_problem do
      return false if device == "unknown" || device.empty?
      # "https://download.lineageos.org"
      base_url = @DISTRIBUTIONS[:LineageOS]
      # e.g. "https://download.lineageos.org/osprey"
      device_url = "#{base_url}/#{device}"

      if http_code(device_url) == "404"
        # Fallback: look for a Rom on the LineageOS for MicroG server
        return dl_lineageos_microg(device, options)
      end

  	  device_page = Nokogiri.HTML(open(device_url))

  	  available = device_page.css("td a").map{|a|
        a['href']}.select{|a| a.end_with?(".zip")}.sort.reverse


      # return latest available build file name in case of a dry run
      if options[:dry_run] == true
        return false unless available[0]
        return available[0].split("/")[-1]
      end

      # Path used on the local file system.
      # e.g. "osprey/lineage-14.1-20180929-microG-osprey.zip"
      file_path = "#{device}/#{available[0].split("/")[-1]}"

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.file?(file_path) && options[:force] != true
        return file_path
      end

      Dir.mkdir(device.to_s) unless Dir.exist?(device.to_s)

      # Put together the full URL for downloading the files.
      # e.g. "https://download.lineage.microg.org/osprey/lineage-14.1-20180929-microG-osprey.zip"
      file_full_href = available[0]

      # download the file and set title of the progress bar
      download(resolve_1(file_full_href), file_path, progressbar: true, title: "LineageOS")

      # return file_path if all worked out and false otherwise
      result = verify_lineageos(file_path, file_full_href) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_lineageos(device, retry: true)
      else
        result
      end
      end
    end

    # Scrape LineageOS_MicroG for devices with downloadable releases.
    # Returns an array with those devices.
    def scrape_lineageos_microg
      retry_on_http_connection_problem do
      page = Nokogiri::HTML(open("https://download.lineage.microg.org"))
      page.css("table a").map{|a| a.content} - ["extra"]
      end
    end

    # Checks whether a LineageOS_MicroG rom is available for given device.
    def lineageos_microg_available(device)
      dl_lineageos_microg(device, dry_run: true)
    end

    def lineageos_microg_parse_version(filename)
      filename.split("-")[1]
    end

    def lineageos_microg_tell_android_version(filename)
      lineageos_tell_android_version filename
    end

    # Download latest LineageOS_MicroG Rom from device URL.
    # Returns relative file path if successful and false otherwise.
    def dl_lineageos_microg(device, options = {})
      retry_on_http_connection_problem do
      return false if device == "unknown" || device.empty?
      # "https://download.lineage.microg.org"
      base_url = "https://download.lineage.microg.org"
      # e.g. "https://download.lineage.microg.org/osprey"
      device_url = "#{base_url}/#{device}"

      return false if http_code(device_url) == "404"

  	  device_page = Nokogiri.HTML(open(device_url))

      # e.g. ["/osprey/lineage-14.1-20180915-microG-osprey.zip", ...]
  	  available = device_page.css("table a").map{|a|
        a['href']}.select{|a| a.end_with?(".zip")}.sort.reverse

      # return latest available build file name in case of a dry run
      if options[:dry_run] == true
        return false if available[0].nil?
        return available[0].split("/")[-1]
      end

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.file?(available[0][1..-1]) && options[:force] != true
        return available[0][1..-1]
      end

      Dir.mkdir(device.to_s) unless Dir.exist?(device.to_s)

      # Extract the most recent Rom and remove the / at the beginning.
      # These paths are used on the local file system.
      # e.g. "osprey/lineage-14.1-20180929-microG-osprey.zip"
  	  file_path = available[0][1..-1]

      # Put together the full URL for downloading the files.
      # e.g. "https://download.lineage.microg.org/osprey/lineage-14.1-20180929-microG-osprey.zip"
      file_full_href = "#{device_url}/#{file_path.split("/")[-1]}"

      # download the file and set title of the progress bar
      download(file_full_href, file_path, progressbar: true, title: "LineageOS_MicroG")

      # return file_path if all worked out and false otherwise
      result = verify(file_path, file_full_href) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_lineageos_microg(device, retry: true)
      else
        result
      end
      end
    end

    # Return available files as array
    def scrape_lineageos_root_addon(device = nil, options = {})
      retry_on_http_connection_problem do
      base_url = @DISTRIBUTIONS[:LineageOS]
      device_url = "#{base_url}/extras"

      return false if http_code(device_url) == "404"

  	  device_page = Nokogiri.HTML(open(device_url))

  	  device_page.css("td a").map{|a|
        a['href']}.select{|a| a.end_with?(".zip")}.sort.reverse
      end
    end

    def lineageos_root_addon_available(device = nil, options = {})
      dl_lineageos_root_addon(device, options.merge({dry_run: true}))
    end

    def lineageos_root_addon_parse_version(filename)
      filename.split("-")[1]
    end

    def lineageos_root_addon_tell_android_version(filename)
      rom_v = lineageos_root_addon_parse_version filename
      maj = rom_v.split(".")[0]
      min = rom_v.split(".")[1..-1].join(".")
      android_v = "#{maj.to_i-7}"
      android_v = "#{android_v}.#{min}" unless min.empty?
    end

    # downloads a rom for a given device from the archive
    # consults the $manager if no rom or filename is given
    def dl_lineageos_root_addon(device = nil, options = {})
      retry_on_http_connection_problem do
      # Need to know which version and which cpu architecture to look for
      return false unless options[:version] && options[:arch]

      available = scrape_lineageos_root_addon.reject{|a| a.include?("remove")}.select{|a|
        a.include?(options[:version].to_s) && a.include?("#{options[:arch].to_s}-")}[0]

      filename = available.split("/")[-1]

      # in case of a dry run: return filename
      return filename if options[:dry_run]

      Dir.mkdir("root-addon") unless Dir.exist?("root-addon")

      file_path = "root-addon/#{filename}"

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.file?(file_path) && options[:force] != true
        return file_path
      end

      # download the file and set title of the progress bar
      download(available, file_path, progressbar: true, title: "LineageOS Root Addon")

      # return file_path if all worked out and false otherwise
      result = verify_lineageos(file_path, available) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_lineageos_root_addon(device, options.merge({retry: true}))
      else
        result
      end
      end
    end

    def scrape_omnirom
      retry_on_http_connection_problem do
      page = Nokogiri::HTML(open(@DISTRIBUTIONS[:Omnirom]))
      page.css("table a").map{|a| a.content}.reject{|e| e.include?(".php")}
      end
    end

    def omnirom_available(device)
      dl_omnirom(device, dry_run: true)
    end

    def omnirom_parse_version(filename)
      generic_parse_version(filename) || filename[/(?<=-)(.*?)(?=-)/]
    end

    def omnirom_tell_android_version(filename)
      omnirom_parse_version filename
    end

    def dl_omnirom(device, options = {})
      retry_on_http_connection_problem do
      return false if device == "unknown" || device.empty?

      base_url = @DISTRIBUTIONS[:Omnirom]
      device_url = "#{base_url}/#{device}"

      return false if http_code(device_url) == "404"

      device_page = Nokogiri.HTML(open(device_url))

      available = device_page.css("table a").map{|a|
        a['href']}.select{|a| a.end_with?(".zip")}.sort.reverse

      # return latest available build file name in case of a dry run
      if options[:dry_run]
        return false if available[0].nil?
        return available[0].split("/")[-1]
      end

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.file?(available[0][1..-1]) && options[:force] != true
        return available[0][1..-1]
      end

      Dir.mkdir(device.to_s) unless Dir.exist?(device.to_s)

      # Extract the most recent Rom and remove the / at the beginning.
      # These paths are used on the local file system.
      file_path = available[0][1..-1]

      # Put together the full URL for downloading the files.
      file_full_href = "#{device_url}/#{file_path.split("/")[-1]}"

      # download the file and set title of the progress bar
      download(file_full_href, file_path, progressbar: true, title: "Omnirom")

      # return file_path if all worked out and false otherwise
      result = verify(file_path, file_full_href) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_omnirom(device, retry: true)
      else
        result
      end
      end
    end

    def scrape_aospextended
      []
    end

    def aospextended_available(device)
      dl_aospextended(device, dry_run: true)
    end

    def aospextended_parse_version(filename)
      generic_parse_version(filename)
    end

    def aospextended_tell_android_version(filename)
      rom_v = aospextended_parse_version filename
      maj = rom_v.split(".")[0]
      "#{maj.to_i+3}"
    end

    def verify_aospextended(file_path, md5)
      return false unless File.exist? file_path
      md5 == Digest::MD5.file(file_path).to_s.strip
    end

    def dl_aospextended(device, options = {})
      retry_on_http_connection_problem do
      return false if device == "unknown" || device.empty?

      base_url = @DISTRIBUTIONS[:AospExtended]
      device_url = "#{base_url}/#{device}"

      versions = ["pie", "oreo", "nougat"]

      api_response = nil
      api_req_threads = {}
      api_responses = []

      versions.each do |version|
        url = "#{device_url}/#{version}"
        # read_timeout set to 10 seconds because the AEX server tends to ignore requests
        api_req_threads[version] = Thread.new {
          JSON.parse(open(url, read_timeout: 10).read) rescue nil
        }
        # api_response = JSON.parse(open(url, read_timeout: 15).read) rescue nil
        # break if api_response
      end

      api_req_threads.values.each {|t| t.join}
      api_responses = api_req_threads.map { |k,v| [k, v.value] }.to_h

      versions.each do |version|
        api_response = api_responses[version] if api_responses[version]
        break if api_responses[version]
      end

      # return latest available build file name in case of a dry run
      if options[:dry_run]
        return false if api_response.nil?
        return api_response["filename"]
      end

      if api_response.nil? || api_response["url"].nil? || api_response["url"].empty?
        log "No response from the AospExtended server. Unable to download the rom."
        return nil
      end

      Dir.mkdir(device.to_s) unless Dir.exist?(device.to_s)

      file_path = "#{device}/#{api_response["filename"]}"

      # Put together the full URL for downloading the files.
      file_full_href = api_response["url"]
      md5 = api_response["md5"]

      # download the file and set title of the progress bar
      download(file_full_href, file_path, progressbar: true, title: "AospExtended")

      # return file_path if all worked out and false otherwise
      result = verify_aospextended(file_path, md5) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_aospextended(device, retry: true)
      else
        result
      end
      end
    end

    def scrape_resurrectionremix
      retry_on_http_connection_problem do
      base_url = @DISTRIBUTIONS[:ResurrectionRemix]
      base_page = Nokogiri.HTML(open(base_url))
      official = base_page.css("li").map{|e| e["data-name"]}.select{|e| e}
      archive_url = "https://archive.resurrectionremix.com"
      archive_page = Nokogiri.HTML(open(archive_url))
      archive = archive_page.css("li").map{|e| e["data-name"]}.select{|e| e}
      all = official + archive
      all.uniq.sort_by(&:downcase)
      end
    end

    def resurrectionremix_available(device)
      dl_resurrectionremix(device, dry_run: true)
    end

    def resurrectionremix_parse_version(filename)
      generic_parse_version(filename)
    end

    def resurrectionremix_tell_android_version(filename)
      rom_v = resurrectionremix_parse_version filename
      maj = rom_v.split(".")[0]
      "#{maj.to_i+2}"
    end

    def dl_resurrectionremix(device, options = {})
      options = {version: "pie"}.merge(options)
      retry_on_http_connection_problem do
      return false if device == "unknown" || device.empty?

      base_url = "#{@DISTRIBUTIONS[:ResurrectionRemix]}/?dir=#{options[:version]}"
      device_url = "#{base_url}/#{device}"

      # Does not give 404 if device page does not exist! Therefore instead:
      if open(device_url).read.include?("File path does not exist")
        # Try older versions of RR. "oreo" and "older"
        case options[:version]
        when "pie" then return dl_resurrectionremix(device, options.merge({version: "oreo"}))
        when "oreo" then return dl_resurrectionremix(device, options.merge({version: "older"}))
        when "older" then return false  # prevent endless recursion
        end
      end

      device_page = Nokogiri.HTML(open(device_url))

      available = device_page.css("li").map{|e|
        e["data-href"]}.select{|e| e && e.end_with?(".zip")}.sort.reverse

      # return latest available build file name in case of a dry run
      if options[:dry_run]
        return false unless available[0]
        return available[0].split("/")[-1]
      end

      # Extract the most recent Rom's file path for local file system.
      file_path = "#{device}/#{available[0].split("/")[-1]}"

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.file?(file_path) && options[:force] != true
        return file_path
      end

      Dir.mkdir(device.to_s) unless Dir.exist?(device.to_s)

      # Put together the full URL for downloading the files.
      file_full_href = "#{base_url}/#{file_path}".gsub("?dir=", "")

      # download the file and set title of the progress bar
      download(file_full_href, file_path, progressbar: true, title: "ResurrectionRemix")

      # return file_path if all worked out and false otherwise
      result = verify(file_path, file_full_href) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_resurrectionremix(device, retry: true)
      else
        result
      end
      end
    end

    def scrape_carbonrom

    end

    def carbonrom_available(device)
      dl_carbonrom(device, dry_run: true)
    end

    def carbonrom_parse_version(filename)
      generic_parse_version(filename)
    end

    def carbonrom_tell_android_version(filename)
      rom_v = carbonrom_parse_version filename
      maj = rom_v.split(".")[0]
      "#{maj.to_i+2}"
    end

    def dl_carbonrom(device, options = {})
      retry_on_http_connection_problem do
      return false if device == "unknown" || device.empty?

      base_url = @DISTRIBUTIONS[:CarbonROM]
      device_url = "#{base_url}/device-#{device}.html"

      return false if http_code(device_url) == "404"

      device_page = Nokogiri.HTML(open(device_url))

      available = device_page.css("table a").map{|a|
        a['href']}.select{|a| a.end_with?(".zip")}.sort.reverse

      # return latest available build file name in case of a dry run
      if options[:dry_run]
        return false if available[0].nil?
        return available[0].split("/")[-1]
      end

      Dir.mkdir(device.to_s) unless Dir.exist?(device.to_s)

      # Extract the most recent Rom and remove the / at the beginning.
      # These paths are used on the local file system.
      file_path = "#{device}/#{available[0].split("/")[-1]}"

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.file?(file_path) && options[:force] != true
        return file_path
      end

      # Put together the full URL for downloading the files.
      file_full_href = available[0]

      # download the file and set title of the progress bar
      download(resolve_1(file_full_href), file_path, progressbar: true, title: "CarbonROM")

      # return file_path if all worked out and false otherwise
      result = verify(file_path, file_full_href) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_carbonrom(device, retry: true)
      else
        result
      end
      end
    end

    def scrape_e

    end

    def e_available(device)
      dl_e(device, dry_run: true)
    end

    def e_parse_version(filename)
      generic_parse_version(filename)
    end

    def e_tell_android_version(filename)
      rom_v = e_parse_version filename
      maj = rom_v.split(".")[0]
      "#{maj.to_i+2}"
    end

    def dl_e(device, options = {})
      retry_on_http_connection_problem do
      return false if device == "unknown" || device.empty?

      base_url = @DISTRIBUTIONS[:e]
      device_url = "#{base_url}/#{device}"

      return false if http_code(device_url) == "404"

      device_page = Nokogiri.HTML(open(device_url))

      available = device_page.css("table a").map{|a|
        a['href']}.select{|a| a.end_with?(".zip")}.sort.reverse

      # return latest available build file name in case of a dry run
      if options[:dry_run]
        return false if available[0].nil?
        return available[0].split("/")[-1]
      end

      Dir.mkdir(device.to_s) unless Dir.exist?(device.to_s)

      # Extract the most recent Rom and remove the / at the beginning.
      # These paths are used on the local file system.
      file_path = "#{device}/#{available[0].split("/")[-1]}"

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.file?(file_path) && options[:force] != true
        return file_path
      end

      # Put together the full URL for downloading the files.
      file_full_href = available[0]

      # download the file and set title of the progress bar
      download(resolve_1(file_full_href), file_path, progressbar: true, title: "CarbonROM")

      # return file_path if all worked out and false otherwise
      result = verify(file_path, file_full_href) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_carbonrom(device, retry: true)
      else
        result
      end
      end
    end

    # def scrape_mockee
    #
    # end
    #
    # def mockee_available(device)
    #   dl_mockee(device, dry_run: true)
    # end
    #
    # def mockee_parse_version(filename)
    #   generic_parse_version(filename)
    # end
    #
    # def verify_mockee(file_path, md5)
    #   return false unless File.exist? file_path
    #   md5 == Digest::MD5.file(file_path).to_s.strip
    # end
    #
    # def mokee_tell_android_version(filename)
    #   rom_v = mokee_parse_version filename
    #   maj = rom_v.split(".")[0]
    #   android_v = "#{maj.to_i+2}"
    # end
    #
    # def dl_mockee(device, options = {})
    #
    # end

    # def scrape_crdroid
    #
    # end
    #
    # def crdroid_available(device)
    #   dl_crdroid(device, dry_run: true)
    # end

    def crdroid_parse_version(filename)
      generic_parse_version(filename)
    end

    # def verify_crdroid(file_path, md5)
    #   return false unless File.exist? file_path
    #   md5 == Digest::MD5.file(file_path).to_s.strip
    # end

    def crdroid_tell_android_version(filename)
      crdroid_parse_version filename
    end

    # def dl_crdroid(device, options = {})
    #
    # end

    def scrape_magisk
      dl_magisk("", dry_run: true)
    end

    def magisk_available(device = "")
      dl_magisk(device, dry_run: true)
    end

    def magisk_parse_version(filename)
      generic_parse_version(filename)
    end

    # No checksums available for download verification
    def dl_magisk(device = "", options = {})
      retry_on_http_connection_problem do
      base_url = @DISTRIBUTIONS[:Magisk]
      return false if http_code(base_url) == "404"

      page = page = Nokogiri.HTML(open(base_url))
      file_full_href = page.search("a").select{|node| node.text.include?("Latest")}[0].attributes["href"].value

      # return latest available build file name in case of a dry run
      if options[:dry_run]
        return false unless file_full_href
        return file_full_href.split("/")[-1]
      end

      Dir.mkdir("root-addon") unless Dir.exist?("root-addon")
      file_path = "root-addon/#{file_full_href.split("/")[-1]}"

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.exist?(file_path) && options[:force] != true
        return file_path
      end

      http_result = download(file_full_href, file_path, progressbar: true, title: "Magisk")
      # cannot verify download: no checksums available upstream

      unless File.exist?(file_path) || options[:retry]
        dl_magisk(device, retry: true)
      else
        http_result.code == "200" ? file_path : nil
      end
      end
    end

    def scrape_nanodroid
      retry_on_http_connection_problem do
      page = Nokogiri::HTML(open(@DISTRIBUTIONS[:NanoDroid]))
      result = page.css("table a").map{|a| a.content}.select { |item| item.start_with? "NanoDroid-" }
      result.select do |item|
        /NanoDroid-\d/.match?(item) && item.end_with?(".zip")
      end
      end
    end

    # no need for device parameter here, but keeping for code coherence
    def nanodroid_available(device = "")
      dl_nanodroid(device, dry_run: true)
    end

    def nanodroid_parse_version(filename)
      generic_parse_version(filename)
    end

    # no need for device parameter here, but keeping for code coherence
    # provide desired modules as hash keys with true value
    # modules: :main, :bromite, :fdroid, :microg, :patcher, :systest, :all
    def dl_nanodroid(device = "", options = {})
      retry_on_http_connection_problem do
      # set default arguments for options hash
      options = {main: true, bromite: true, patcher: true}.merge(options)

      base_url = @DISTRIBUTIONS[:NanoDroid]

      return false if http_code(base_url) == "404"

      # check available files and filter out the correct one
      available = scrape_nanodroid.sort

      # name of the file to download
      file_path = available[0]

      # return latest available build file name in case of a dry run
      if options[:dry_run]
        return false if file_path.nil?
        return file_path
      end

      # Put together the full URL for downloading the files.
      file_full_href = "#{base_url}/#{file_path}"

      # download the file and set title of the progress bar
      # download(file_full_href, file_path, progressbar: true, title: "NanoDroid")

      # put together the desired modules into a hash
      modules = {
        main: options[:main],
        bromite: options[:bromite],
        fdroid: options[:fdroid],
        microg: options[:microg],
        patcher: options[:patcher],
        systest: options[:systest]
      }.compact

      # download and verify the modules
      result = dl_nanodroid_modules(file_full_href, modules) if modules.any?{|k,v| v}

      # retry download if the result is negative
      unless result || options[:retry]
        dl_nanodroid(device, retry: true)
      else
        result
      end
      end
    end

    # called by dl_nanodroid with file_full_href as argument
    # downloads the desired modules given as hash keys with true value
    def dl_nanodroid_modules(file_full_href, modules = {})
      Dir.mkdir("NanoDroid") unless Dir.exist?("NanoDroid")
      result = {}
      if modules[:all]
        result[:main] = dl_nanodroid_modules file_full_href, {main: true}
        result[:bromite] = dl_nanodroid_modules file_full_href, {bromite: true}
        result[:fdroid] = dl_nanodroid_modules file_full_href, {fdroid: true}
        result[:microg] = dl_nanodroid_modules file_full_href, {microg: true}
        result[:patcher] = dl_nanodroid_modules file_full_href, {patcher: true}
        result[:systest] = dl_nanodroid_modules file_full_href, {systest: true}
        # result = main && bromite && fdroid && microg && patcher && systest ? true : false
      end
      if modules[:main]
        file_path = "NanoDroid/#{File.basename(file_full_href)}"
        download(file_full_href, file_path, progressbar: true, title: "NanoDroid")
        result[:main] = verify(file_path, file_full_href) ? file_path : (File.delete(file_path); false)
      end unless modules[:all]
      if modules[:bromite]
        new_href = file_full_href.gsub("NanoDroid-", "NanoDroid-BromiteWebView-")
        file_path = "NanoDroid/#{File.basename(new_href)}"
        download(new_href, file_path, progressbar: true, title: "NanoDroid-BromiteWebView")
        result[:bromite] = verify(file_path, new_href) ? file_path : (File.delete(file_path); false)
      end unless modules[:all]
      if modules[:fdroid]
        new_href = file_full_href.gsub("NanoDroid-", "NanoDroid-fdroid-")
        file_path = "NanoDroid/#{File.basename(new_href)}"
        download(new_href, file_path, progressbar: true, title: "NanoDroid-fdroid")
        result[:fdroid] = verify(file_path, new_href) ? file_path : (File.delete(file_path); false)
      end unless modules[:all]
      if modules[:microg]
        new_href = file_full_href.gsub("NanoDroid-", "NanoDroid-microG-")
        file_path = "NanoDroid/#{File.basename(new_href)}"
        download(new_href, file_path, progressbar: true, title: "NanoDroid-microG")
        result[:microg] = verify(file_path, new_href) ? file_path : (File.delete(file_path); false)
      end unless modules[:all]
      if modules[:patcher]
        new_href = file_full_href.gsub("NanoDroid-", "NanoDroid-patcher-")
        file_path = "NanoDroid/#{File.basename(new_href)}"
        download(new_href, file_path, progressbar: true, title: "NanoDroid-patcher")
        result[:patcher] = verify(file_path, new_href) ? file_path : (File.delete(file_path); false)
      end unless modules[:all]
      if modules[:systest]
        new_href = file_full_href.gsub("NanoDroid-", "NanoDroid-systest-")
        file_path = "NanoDroid/#{File.basename(new_href)}"
        download(new_href, file_path, progressbar: false, title: "NanoDroid-systest")
        result[:systest] = verify(file_path, new_href) ? file_path : (File.delete(file_path); false)
      end unless modules[:all]
      # return true if no modules desired
      result
    end

    def scrape_nanodroid_bromite
      scrape_nanodroid.select { |item| item.include?("BromiteWebView") }
    end

    # no need for device parameter here, but keeping for code coherence
    def nanodroid_bromite_available(device = "")
      nanodroid_available(device).gsub("NanoDroid-", "NanoDroid-BromiteWebView-")
    end

    def nanodroid_bromite_parse_version(filename)
      nanodroid_parse_version(filename)
    end

    def dl_nanodroid_bromite(device = "", options = {})
      dl_nanodroid(device, {main: nil, bromite: true, fdroid: nil, microg: nil,
                            patcher: nil, systest: nil})
    end

    def scrape_nanodroid_fdroid
      scrape_nanodroid.select { |item| item.include?("fdroid") }
    end

    # no need for device parameter here, but keeping for code coherence
    def nanodroid_fdroid_available(device = "")
      nanodroid_available(device).gsub("NanoDroid-", "NanoDroid-fdroid-")
    end

    def nanodroid_fdroid_parse_version(filename)
      nanodroid_parse_version(filename)
    end

    def dl_nanodroid_fdroid(device = "", options = {})
      dl_nanodroid(device, {main: nil, bromite: nil, fdroid: true, microg: nil,
                            patcher: nil, systest: nil})
    end

    def scrape_nanodroid_microg
      scrape_nanodroid.select { |item| item.include?("microG") }
    end

    # no need for device parameter here, but keeping for code coherence
    def nanodroid_microg_available(device = "")
      nanodroid_available(device).gsub("NanoDroid-", "NanoDroid-microG-")
    end

    def nanodroid_microg_parse_version(filename)
      nanodroid_parse_version(filename)
    end

    def dl_nanodroid_microG(device = "", options = {})
      dl_nanodroid(device, {main: nil, bromite: nil, fdroid: nil, microg: true,
                            patcher: nil, systest: nil})
    end

    def scrape_nanodroid_patcher
      scrape_nanodroid.select { |item| item.include?("patcher") }
    end

    # no need for device parameter here, but keeping for code coherence
    def nanodroid_patcher_available(device = "")
      nanodroid_available(device).gsub("NanoDroid-", "NanoDroid-patcher-")
    end

    def nanodroid_patcher_parse_version(filename)
      nanodroid_parse_version(filename)
    end

    def dl_nanodroid_patcher(device = "", options = {})
      dl_nanodroid(device, {main: nil, bromite: nil, fdroid: nil, microg: nil,
                            patcher: true, systest: nil})
    end

    def scrape_nanodroid_systest
      scrape_nanodroid.select { |item| item.include?("systest") }
    end

    # no need for device parameter here, but keeping for code coherence
    def nanodroid_systest_available(device = "")
      nanodroid_available(device).gsub("NanoDroid-", "NanoDroid-systest-")
    end

    def nanodroid_systest_parse_version(filename)
      nanodroid_parse_version(filename)
    end

    def dl_nanodroid_systest(device = "", options = {})
      dl_nanodroid(device, {main: nil, bromite: nil, fdroid: nil, microg: nil,
                            patcher: nil, systest: true})
    end


    ################## archive methods ##################

    def scrape_archive
      retry_on_http_connection_problem do
      base_page = Nokogiri.HTML(open(@ARCHIVE).read)
      base_page.css("a").map{|a| a['href']}[1..-1].map{|e| e[0..-2]}
      end
    end

    # Return a hash with subfolders as keys and available files as array
    def archive_available(device = nil, options = {})
      retry_on_http_connection_problem do
      options = {subfolder: nil, override: false, startuplogo: false, flashme: false}.merge(options)
      device = $device.codename unless device unless $cli
      return false if device == "unknown" || device.empty?

      base_url = @ARCHIVE
      device_url = "#{base_url}/#{device}"
      device_url = "#{base_url}/#{device}/#{options[:subfolder]}" if options[:subfolder]

      return {} if http_code(device_url) == "404"

      ### .read is just a temporary fix until nokogiri update for jruby! ###
      device_page = Nokogiri.HTML(open(device_url).read)

      available = device_page.css("a").map{|a| a['href']}[1..-1]

      # kick out md5 and sha256 (and override and additional files)
      available.reject!{|item| item.end_with?("md5","sha256","md5sum","sha256sum")}
      available.reject!{|item| item.start_with?("flashme_pre")} unless options[:flashme]
      available.reject!{|item| item.start_with?("flashme_post")} unless options[:flashme]
      available.reject!{|item| item.start_with?("override")} unless options[:override]
      available.reject!{|item| item.start_with?("startup-logo")} unless options[:startuplogo]

      if options[:subfolder]
        available
      else
        # get content of subfolders as nested arrays
        available.map do |sf|
          [sf[0..-2], archive_available(device, {subfolder: sf, override: options[:override], flashme: options[:flashme]})]
        end.to_h
      end
    end rescue {} # return empty hash if the server is not reachable
    end

    # returns a hash with the latest (alphabetic last?) available files for each subfolders
    def archive_latest_available(device, options = {})
      options = {override: true, flashme: true}.merge(options)
      return false if device == "unknown" || device.empty?
      archive_available(device, options).map do |k,v|
        # give not only latest but all files for flashme_pre and flashme_post
        [k.to_sym, k.to_s.start_with?("flashme_") ? v : v.reverse[0]]
      end.to_h
    end

    def archive_available_versions(device)
      result = {}
      archive_latest_available(device).each do |key, value|
        if value == false
          result[key] = nil
        else
          if respond_to? "#{key}_parse_version".downcase.to_sym
            result[key] = send("#{key}_parse_version".downcase, value)
          else
            result[key] = generic_parse_version.downcase value
          end
        end
      end
      result
    end
    alias archive_latest_available_versions archive_available_versions

    def archive_parse_version(filename)
      generic_parse_version(filename)
    end

    # downloads a rom for a given device from the archive
    # consults the $manager if no rom or filename is given
    def dl_archive(device = nil, rom = nil, filename = nil, options = {})
      retry_on_http_connection_problem do
      device = $device.codename unless device unless $cli
      return false if device == "unknown" || device.empty?

      base_url = @ARCHIVE
      # e.g. "https://archive.free-droid.com/osprey"
      device_url = "#{base_url}/#{device}"
      if rom.nil?
        $manager.user_choice[:rom] ? rom = $manager.user_choice[:rom] : rom = $manager.suggest[:rom]
      end
      rom_url = "#{device_url}/#{rom}"

      if filename.nil? || filename.empty?
        filename = archive_latest_available(device)[rom.to_sym]
      end
      file_url = "#{rom_url}/#{filename}"

      # in case of a dry run: return filename if file is downloadable
      # if options[:dry_run] == true
      #
      # end

      Dir.mkdir(device.to_s) unless Dir.exist?(device.to_s)

      file_path = "#{device}/#{filename}"

      # return local relative file path if the file already exists
      # and the options[:force] flag is not set to true
      if File.file?(file_path) && options[:force] != true
        return file_path
      end

      # Put together the full URL for downloading the files.
      file_full_href = file_url

      # download the file and set title of the progress bar
      download(file_full_href, file_path, progressbar: true, title: "#{rom}")

      # return file_path if all worked out and false otherwise
      result = verify(file_path, file_full_href) ? file_path : (File.delete(file_path); false)

      # retry download if a checksum explicitely differered
      unless File.exist?(file_path) || options[:retry]
        dl_archive(device, rom, filename, retry: true)
      else
        result
      end
      end
    end

    def startup_logo(device, target_path = "#{device}/startup-logo.bin")
      retry_on_http_connection_problem do
      return false if device == "unknown" || device.empty?
      return target_path if File.exist?(target_path)
      available = archive_available(device, startuplogo: true)["startup-logo"]
      if available
        url = "#{@ARCHIVE}/#{device}/startup-logo/#{available[0]}"
        download(url, target_path, progressbar: false, title: "startup-logo")
      end
      File.exist?(target_path) ? target_path : nil
      end
    end

    def flashme(device, files = nil)
      retry_on_http_connection_problem do
      unless files && files.is_a?(Hash)
        files = {}
        archive_available = archive_latest_available(device)
        files[:pre] = archive_available[:flashme_pre]
        files[:post] = archive_available[:flashme_post]
      end
      result = {}
      result[:pre] = if files[:pre].nil? || files[:pre].empty?
        []
      else
        flashme_pre(device, files[:pre])
      end
      result[:post] = if files[:post].nil? || files[:post].empty?
        []
      else
        flashme_post(device, files[:post])
      end
      result
      end
    end

    def flashme_pre(device, files = nil, target_dir = "#{device}/pre")
      Dir.mkdir(target_dir) unless Dir.exist?(target_dir)
      files = Get.archive_latest_available(device)[:flashme_pre] unless files
      files.map do |filename|
        url = "#{@ARCHIVE}/#{device}/flashme_pre/#{filename}"
        target_path = "#{target_dir}/#{filename}"
        download(url, target_path, progressbar: false, title: "flashme-pre")
        File.exist?(target_path) ? target_path : false
      end
    end

    def flashme_post(device, files = nil, target_dir = "#{device}/post")
      Dir.mkdir(target_dir) unless Dir.exist?(target_dir)
      files = Get.archive_latest_available(device)[:flashme_post] unless files
      files.map do |filename|
        url = "#{@ARCHIVE}/#{device}/flashme_post/#{filename}"
        target_path = "#{target_dir}/#{filename}"
        download(url, target_path, progressbar: false, title: "flashme-post")
        File.exist?(target_path) ? target_path : false
      end
    end

    def helpme
    	require "mechanize"
    	agent = Mechanize.new
    	agent.get "https://twrp.me/Devices/Samsung/"
    	agent.page.links[6..-4].each do |link|
    		begin
          model, device = nil, nil
    			twrp_page = link.click
    			tree = twrp_page.links.find{ |l| l.text.include?("Device Tree") }.click
          ["system", "_prop", "prop.", "full_", "omni_", "lineage"].each do |try|
            propfile = tree.links.find{ |l| l.text.include?(try) &&
              (l.text.end_with?(".mk") || l.text.end_with?("prop"))}.click rescue next
            raw = propfile.links.find{ |l| l.text.include?("Raw")}.click rescue next
            data = raw.body
            lookup = data.lines.select{|l| l.count("=") == 1 &&
              (l.strip.start_with?("ro") || l.strip.start_with?("PRODUCT"))}.map {|l|
              l.strip.split("=").map{|k,v| k.include?(" ") ? k.split(" ")[0].strip : k.strip}}.to_h
            model = if lookup['ro.product.model']
                      lookup['ro.product.model']
                    elsif lookup['PRODUCT_MODEL']
                      lookup['PRODUCT_MODEL']
                    else nil
                    end
            device =  if lookup['ro.product.device']
                        lookup['ro.product.device']
                      elsif lookup['PRODUCT_DEVICE']
                        lookup['PRODUCT_DEVICE']
                      elsif lookup['PRODUCT_RELEASE_NAME']
                        lookup['PRODUCT_RELEASE_NAME']
                      elsif lookup['PRODUCT_NAME']
                        lookup['PRODUCT_NAME'].start_with?("omni_") ? lookup['PRODUCT_NAME'][5..-1] : lookup['PRODUCT_NAME']
                      else nil
                      end
            break if model && device
          end
    			if model && device && (model.strip != device.strip)
    				File.open("helping.yml", "a") do |file|
    					file.puts "#{model.strip}: #{device.strip}"
    				end
    				puts "#{model.strip}: #{device.strip}"
          else raise "Nothing found."
    			end
    		rescue Exception => e
          begin
            puts e.message
      			puts "skipped #{link.text}"
            case
            when (data rescue nil) then (lookup ? (puts lookup) : (puts data))
            when (propfile rescue nil) then puts propfile.uri
            when (tree rescue nil) then puts tree.uri
            else puts twrp_page.uri
            end rescue nil
            # binding.pry
          rescue
            nil
          end
    		end
    	end
    end

    def retry_on_http_connection_problem(retries = 3)
      try = 0
      begin
        yield
      rescue Errno::ECONNREFUSED, Net::ReadTimeout, Net::OpenTimeout,
        SocketError, OpenSSL::SSL::SSLError => e
        if (try += 1) <= retries
          log "HTTP connection error: #{e.message} - retrying..."
          sleep(try)
          retry
        else
          raise e
        end
      end
    end

  end # class << self
end # module
