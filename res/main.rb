#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative "sentry"
require_relative "log"
require_relative "adb"
require_relative "twrp"
require_relative "fastboot"
require_relative "heimdall"
require_relative "notifier"
require_relative "device"
require_relative "os"
require_relative "get"
require_relative "manager"
require_relative "reporter"
require_relative "updater"

module Main

extend Log  # make log() method available as shortcut for Log.add()

# Flags to let the Notifier know if and where we are in the precodure of flashing
@@flashing = false

$flash_progress = {unlock: false, twrp: false, update_twrp: false, format_data: false,
                    rest: false, flashme_pre: false, rom: false, root: false,
                    nanodroid: false, patcher: false, flashme_post: false}

# Hash storing relative paths to TWRP, Rom, etc. for flashing
@@files = {twrp_file_path: nil, rom_file_path: nil, root_file_path: nil,
            nanodroid_file_path: nil, nanodroid_bromite_file_path: nil,
            nanodroid_patcher_file_path: nil}

class << self

  # shortcut for faking a device connection for development purposes
  def test(name = "i9305", brand = "samsung")
    $device.name = name
    $device.codename = Find.codename name
    $device.brand = brand
    $device.notifier.detected_new_device
    $device.notifier.update($device, "android")
    $manager
  end

  def flashing?
    @@flashing
  end

  def files
    @@files
  end

  def got_binaries?
    if OS.which == "windows"
      File.file?("bin/fastboot.exe") && File.file?("bin/heimdall.exe") &&
      File.file?("bin/adb.exe") && File.file?("bin/AdbWinApi.dll") &&
      File.file?("bin/AdbWinUsbApi.dll") && File.file?("bin/libgcc_s_seh-1.dll") &&
      File.file?("bin/libstdc++-6.dll") && File.file?("bin/libusb-1.0.dll") &&
      File.file?("bin/libwinpthread-1.dll")
    else
      File.file?("bin/fastboot") && File.file?("bin/heimdall") && File.file?("bin/adb")
    end
  end

  def cleanup_after_update
    bin_dir = Find.bin_dir
    File.delete "#{bin_dir}/Free-Droid.old" if File.exist? "#{bin_dir}/Free-Droid.old"
    File.delete "#{bin_dir}/update.sh" if File.exist? "#{bin_dir}/update.sh"
    File.delete "#{bin_dir}/update.bat" if File.exist? "#{bin_dir}/update.bat"
    File.delete "update-script.rb" if File.exist? "update-script.rb"
  end

  # Update application if an update is available
  # Download the fastboot, heimdall and adb binaries if missing one of them
  def setup
    log "----------------"  # for better visual feedback in the log
    Dir.mkdir("log") unless Dir.exist?("log")
    Log.session # reads older session if one exists
    Log.read_versions_of_binaries # for error reporting
    Dir.mkdir("bin") unless Dir.exist?("bin")
    cleanup_after_update
    if Get.internet_connection?("https://archive.free-droid.com") &&
      Get.internet_connection?("https://gitlab.com/free-droid/free-droid/raw/master/lookups/codenames.yml")
      Gui.do :startup_internet_connection, true
      servers_reachable = true
    else
      Gui.do :startup_internet_connection, false
      # Gui.do :close_app, "No connection to the server."
      Gui.do :setup_failure, "No connection to the server. Retry?"
      return false
    end
    if Updater.new_version_available?
      Gui.do :startup_app_up_to_date, false
      Updater.try_update
      return false
    else
      Gui.do :startup_app_up_to_date, true
      uptodate = true
    end
    if got_binaries?
      Gui.do :startup_got_binaries, true
    else
      Gui.do :startup_got_binaries, false
      got_them = Get.get(:Platform_Tools) && Get.get(:Heimdall)
      Gui.do :startup_got_binaries, true if got_them
    end
    result = servers_reachable && uptodate && got_binaries?
    result ? setup_success : Gui.do(:close_app, "Unable to prepare the application.")
  end

  def setup_success
    log "Setup successful: Free-Droid #{VERSION} on #{OS.which} with CPU: #{OS.cpu}"
    Gui.do :startup_linux_sudo_notice if OS.linux?
    ADB.start_server
    $device = Device.new
    $device.observe
    $manager = Manager.new
    $reporter = Reporter.new
    $reporter.report progress: "setup-successful"
    Gui.do :startup_done
    if ARGV.length >= 1 # developer mode
      require "pry-debugger-jruby"
      binding.pry
      puts
    end
  end

  def install_adb_driver
    installer = Get.adb_driver
    if installer && File.exist?(installer)
      # put together the command to launch the installer
      command = "Powershell -Command \"& { Start-Process \"#{File.basename(installer)}\" -Verb RunAs }"
      # launch the installer
      Dir.chdir(File.dirname(installer)) do
        execute command
      end
    else
      Gui.do :alert, "Downloading the driver installer failed. Please try by yourself - Let me open the website for you..."
      sleep 10
      OS.open_browser "https://universaladbdriver.com/"
    end
  end

  def read_settings_from_gui
    @@settings = {}
    @@settings[:use_own_rom] = $chk_use_own_rom_state
    @@settings[:magisk] = $chk_magisk_state
    @@settings[:fdroid] = $chk_fdroid_state
    @@settings[:microg] = $chk_microg_state
    @@settings[:aurora] = $chk_aurora_state
    @@settings[:reflash_twrp] = $chk_reflash_twrp_state
    @@settings[:dirty_flash] = $chk_dirty_flash_state
    @@settings[:only_flash_my_zip] = $chk_only_flash_my_zip_state
    @@settings[:sigspoof] = $chk_sigspoof_state
    @@settings[:swype_libs] = $chk_swype_libs_state
    @@settings[:gsync] = $chk_gsync_adapters_state
    @@settings
  end

  def reset
    @@flashing = false
    $aborting = false
    $flash_progress = $flash_progress.map{|k,v| [k,false]}.to_h
    @@files = {twrp_file_path: nil, rom_file_path: nil, root_file_path: nil,
                nanodroid_file_path: nil, nanodroid_bromite_file_path: nil,
                nanodroid_patcher_file_path: nil,
                flashme_file_paths: {pre: [], post: []}}
    $manager.reset
    Gui.do :reset
    # Gui.do :btn_start_deactivate
    # Gui.do :btn_abort_visible, false
    # Gui.do :btn_menu_click, 1   # unlock the gui menu
    # Gui.do :reset_progressbar, ""
    # $device.notifier.reset_rom_choice_gui
    $device.notifier.update
  end

  def abort
    @@flashing = false
    Gui.do :btn_menu_click, 1   # unlock the gui menu
    $aborting = true
    log "User hit abort! Procedure will stop after the current step."
    Gui.do :update_instructions, "Aborting...\n\nProcedure will stop after\nthe current step."
  end

  # Launch flashing procedure
  def flash
    unless $device.custom_rom? || $device.know_how_to_unlock?
      Gui.do :update_instructions, "Sorry, I don't know how to work with your device."
      return false
    end
    $reporter.report progress: "start"
    Thread.new do log_on_fail(quit: false) do
      Gui.do :btn_start_deactivate
      Gui.do :lock_menu_buttons
      # Gui.do :btn_abort_visible, true
      Gui.do :update_instructions, "Preparing...\n\nPlease wait."
      read_settings_from_gui
      # Download everything we need and save the relative paths to their files
      @@files = $manager.get_files $device.codename, @@settings
      # Abort if we do not have what we need
      if @@files.all?{ |k,v| v }
        log "Starting new flashing procedure."
        Gui.do :update_instructions, "Working..."
        Gui.do :update_progress_title, "Starting installation..."
        # Set flashing flag to let the Notifier know we are in the procedure
        @@flashing = true
        # make sure not to loose sudo while flashing under linux:
        Thread.new {while Main.flashing?; `sudo -v`; sleep 9; end} if OS.linux?
        flash_conductor
      else
        @@flashing = false
        Gui.do :btn_menu_click, 1   # unlock the gui menu
        Gui.do :update_instructions, "Something went wrong.\n\nAre you connected to the internet?"
        log "Unable to retrieve TWRP and the Rom."
        Gui.do :btn_start_activate
        Gui.do :btn_abort_visible, false
        exit
      end
    end end # log_on_fail and thread
  end

  # Orchestrate the flashing procedure
  def flash_conductor(state = nil)
    log_on_fail(quit: false, info: true) do
    return nil unless @@flashing
    return nil if state == "sideload" || state == "unauthorized"
    state = $device.state if state.nil?
    log "Call to flash_conductor with device state: #{state}"
    log "Progress is: #{$flash_progress}"
    log "Checking progress..."
    check_progress(state)
    log "Progress is: #{$flash_progress}"
    return false unless flashing?
    return false if $flash_progress.any?{|k,v| v == "WIP"}
    if $flash_progress[:unlock] == false
      $flash_progress[:unlock] = "WIP"
      unlock(state)
    elsif $flash_progress[:twrp] == false
      $flash_progress[:twrp] = "WIP"
      flash_twrp(state)
    elsif $flash_progress[:update_twrp] == false
      $flash_progress[:update_twrp] = "WIP"
      update_twrp(state)
    elsif $flash_progress[:format_data] == false
      $flash_progress[:format_data] = "WIP"
      format_data(state)
    elsif $flash_progress[:rest] == false  # will call flash procedures for the rest
      $flash_progress[:rest] = "WIP"
      # flash_rom(state)
      flash_rest(state)
    # elsif $flash_progress[:root] == false
    #   $flash_progress[:root] = "WIP"
    #   flash_root(state)
    # elsif $flash_progress[:nanodroid] == false
    #   $flash_progress[:nanodroid] = "WIP"
    #   flash_nanodroid(state)
    # elsif $flash_progress[:patcher] == false
    #   $flash_progress[:patcher] = "WIP"
    #   flash_patcher(state)
    else
      Gui.do :update_progress_title, "Installation complete."
      Gui.do :progressbar_set, 1
      Gui.do :update_instructions, "Finished.\n\nNotice: First boot will take longer!"
      log "Finished."
      @@flashing = false
      Gui.do :btn_menu_click, 1   # unlock the gui menu
      log "Progress is: #{$flash_progress}"
      if $flash_progress.values.all?
        $reporter.report action: "success", name: File.basename(@@files[:rom_file_path]), progress: "finished"
      else
        failed_at = $flash_progress.select { |k,v| !v }.keys.first
        $reporter.report action: "failed at #{failed_at}", name: File.basename(@@files[:rom_file_path]), progress: "finished"
      end
      $device.reboot
      sleep 20
      reset
    end
    end # log_on_fail
  end

  @@progress_steps = 10
  @@progress_steps = @@progress_steps.to_f

  def check_progress(state = nil)
    state = $device.state if state.nil?
    if $flash_progress[:unlock] == false
      if $device.brand.downcase == "samsung"
        $flash_progress[:unlock] = true
        Gui.do :progressbar_set, 1 / @@progress_steps
        log "No bootloader unlock needed on Samsung devices."
      elsif $device.custom_rom?
        $flash_progress[:unlock] = true
        Gui.do :progressbar_set, 1 / @@progress_steps
        log "Detected custom rom. Assuming the bootloader is already unlocked."
      elsif $chk_skip_bootloader_unlock_state
        $flash_progress[:unlock] = true
        Gui.do :progressbar_set, 1 / @@progress_steps
        log "User chose to skip unlocking the bootloader."
      end
    end
    if $flash_progress[:unlock] == "WIP"
      if $device.adb?
        $flash_progress[:unlock] = true
        Gui.do :progressbar_set, 1.7 / @@progress_steps
        Gui.do :update_instructions, "Well done!\n\nRebooting again...\nPlease wait."
        $device.reboot "bootloader"
      end
    end
    # if $flash_progress[:boot_logo] == false
    #   $flash_progress[:boot_logo] = true unless $device.brand.downcase == "motorola"
    # end
    if @@settings[:only_flash_my_zip]
      # do we need to do something at this point?
    end
    if $flash_progress[:twrp] == false
      if $device.custom_rom?
        $flash_progress[:twrp] = true
        Gui.do :progressbar_set, 2 / @@progress_steps
        log "Detected custom rom. Assuming TWRP already installed."
      end unless @@settings[:reflash_twrp]
    end
    if $flash_progress[:twrp] == false
      if $device.state == "recovery" && TWRP.version_connected
        $flash_progress[:twrp] = true
        Gui.do :progressbar_set, 2 / @@progress_steps
        log "TWRP is running."
        unless TWRP.need_update?
          $flash_progress[:update_twrp] = true
          Gui.do :progressbar_set, 3 / @@progress_steps
          log "TWRP has the correct version."
        end
      end unless @@settings[:reflash_twrp]
    end
    if $flash_progress[:twrp] == "WIP"
      TWRP.wait_untill_ready if state == "recovery"
      if $device.state == "recovery" && TWRP.version_connected
        $flash_progress[:twrp] = true
        Gui.do :progressbar_set, 2 / @@progress_steps
        Gui.do :update_instructions, "Well done!\n\nPlease wait for the installation to finish."
        log "Congratulations! TWRP successfully installed."
        unless TWRP.need_update?
          $flash_progress[:update_twrp] = true
          Gui.do :progressbar_set, 3 / @@progress_steps
          log "TWRP has the correct version."
        end
      end
    end
    if $flash_progress[:update_twrp] == false
      if $device.state == "recovery" && TWRP.version_connected
        unless TWRP.need_update?
          $flash_progress[:update_twrp] = true
          Gui.do :progressbar_set, 3 / @@progress_steps
          log "TWRP has the correct version."
        end
      end
    end
    if $flash_progress[:update_twrp] == "WIP"
      if $device.state == "recovery" && TWRP.version_connected
        if TWRP.need_update?
          $flash_progress[:update_twrp] = false
          log "Updating TWRP failed."
        else
          $flash_progress[:update_twrp] = true
          Gui.do :progressbar_set, 3 / @@progress_steps
          log "Successfully updated TWRP to correct version."
        end
      end
    end
    if $flash_progress[:format_data] == false
      if @@settings[:dirty_flash]
        $flash_progress[:format_data] = true
        Gui.do :progressbar_set, 3.5 / @@progress_steps
        log "User chose to dirty flash. Skipping formatting data partition."
      elsif $device.state == "recovery" && TWRP.version_connected
        if TWRP.data_mountable?
          $flash_progress[:format_data] = true
          Gui.do :progressbar_set, 3.5 / @@progress_steps
          log "Data partition is mountable. No need to format."
        end
      end
    end
    if $flash_progress[:format_data] == "WIP"
      sleep 2
      if $device.state == "recovery" && TWRP.version_connected
        if TWRP.data_usable?
          $flash_progress[:format_data] = true
          Gui.do :progressbar_set, 3.5 / @@progress_steps
          log "Successfully formatted data partition."
        else
          $flash_progress[:format_data] = false
          log "Formatting data partition failed."
        end
      end
    end
  end

  def unlock(state = nil)
    return false if $aborting || !@@flashing
    $reporter.report progress: "unlock"
    state = $device.state if state.nil?
    if $device.know_how_to_unlock?
      Gui.do :update_instructions, "1. Step: Bootloader unlock."
      unlock_result = $device.unlock
      if unlock_result == "already unlocked"
        $flash_progress[:unlock] = true
        Gui.do :progressbar_set, 1.3 / @@progress_steps
        Gui.do :update_instructions, "Bootloader already unlocked. Trying to go directly to the next step..."
        flash_conductor "fastboot"
      elsif unlock_result
        $flash_progress[:unlock] = true
        Gui.do :progressbar_set, 1.3 / @@progress_steps
        # observe if device disconnects and (probably) reboots itself
        if observe_reboot_after_unlock?
          Gui.do :update_instructions, "Your device should reset and reboot now.\n\nPlease enable USB-debugging again."
        else
          flash_conductor $device.state
        end
      else
        $flash_progress[:unlock] = false
        @@flashing = false
        Gui.do :btn_menu_click, 1   # unlock the gui menu
      end
      $reporter.report progress: "unlocked" if $flash_progress[:unlock]
    else
      Gui.do :update_instructions, "Sorry,\n\nyour device is not supported (yet)."
      @@flashing = false
      Gui.do :btn_menu_click, 1   # unlock the gui menu
    end
  end

  def observe_reboot_after_unlock?
    sleep 6
    case $device.brand.downcase
    when "motorola", "nvidia" then $device.state != "fastboot"
    when "oneplus" then true  # user needs to confirm on device screen
    else $device.state != "fastboot"
    end
  end

  def flash_twrp(state = nil)
    return false if $aborting || !@@flashing
    $reporter.report progress: "flash-twrp"
    state = $device.state if state.nil?
    if ["heimdall", "fastboot"].include?(state) || $device.wait_for_bootloader
      # for motorola: try to replace boot logo
      if $device.brand.downcase == "motorola"
        $device.fastboot.flash_startup_logo
        sleep 2
      end
      Gui.do :update_progress_title, "Installing TWRP..."
      if $device.flash_recovery(@@files[:twrp_file_path])
        # Update GUI: Waiting for recovery
        Gui.do :update_instructions, "Trying to reboot to TWRP..."
        Gui.do # this one is necessary for correct gui display of preceding notice
        unless $device.brand.downcase == "sony"
          Gui.do :alert, "IMPORTANT:\n\n#{Find.recovery_key_combination($device.codename)}"
        end
      else # failure
        Gui.do :update_instructions, "Failed."
        log "Failed to flash TWRP. Aborting flash procedure."
        reset
      end
    else
      log "No connection to bootloader"
      Gui.do :update_progress_title, "Rebooting to bootloader..."
      $device.reboot("bootloader")
    end
  end

  def update_twrp(state = nil)
    return false if $aborting || !@@flashing
    $reporter.report progress: "update-twrp"
    state = $device.state if state.nil?
    if state == "recovery"
      Gui.do :update_progress_title, "Updating TWRP..."
      TWRP.update
    else
      Gui.do :update_progress_title, "Getting back to TWRP..."
      $flash_progress[:update_twrp] = false
      $device.reboot("recovery")
    end
  end

  def format_data(state = nil)
    return false if $aborting || !@@flashing
    $reporter.report progress: "format-data"
    state = $device.state if state.nil?
    if state == "recovery" && $device.wait_for_twrp
      Gui.do :update_progress_title, "Formatting data partition..."
      # success = TWRP.format_data
      success = false
      if success
        $device.reboot("recovery")
      else
        Gui.do :update_instructions, "Unable to format the data partition!\n\nOn your device please select\n'Wipe', then 'Format Data'\nand confirm by typing 'yes'."
        # watch the TWRP log to see when the user is done
        loop do
          sleep 5
          if !TWRP.connected?
            break
          elsif TWRP.get_and_read_log(30).any?{|line| line.include?("You may need to reboot recovery")}
            sleep 1
            $device.reboot("recovery")
            Gui.do :update_instructions, "Very good!\n\nRebooting into TWRP..."
            break
          end
        end
      end
    else
      Gui.do :update_progress_title, "Getting back to TWRP..."
      $device.reboot("recovery")
    end
  end

  def flash_rest(state = nil)
    return false if $aborting || !@@flashing
    if @@settings[:only_flash_my_zip]
      log "Flashing only selected rom/zip..."
      flash_rom(state)
      sleep 2
      $flash_progress[:rest] = true
    else
      sleep 2
      state = $device.state if state.nil?
      log "Performing a systest..."
      flash_systest(state, "systest-before.log")
      Gui.do :progressbar_set, 4 / @@progress_steps
      sleep 2
      return false if $aborting || !@@flashing
      log "Flashing additional files pre-rom..."
      flash_pre(state)
      sleep 2
      return false if $aborting || !@@flashing
      log "Flashing rom:"
      flash_rom(state)
      Gui.do :progressbar_set, 5 / @@progress_steps
      sleep 2
      return false if $aborting || !@@flashing
      log "Flashing root addon:"
      flash_root(state)
      Gui.do :progressbar_set, 6 / @@progress_steps
      sleep 2
      return false if $aborting || !@@flashing
      log "Flashing nanodroid:"
      flash_nanodroid(state)
      Gui.do :progressbar_set, 7 / @@progress_steps
      sleep 2
      return false if $aborting || !@@flashing
      log "Flashing patcher:"
      flash_patcher(state)
      Gui.do :progressbar_set, 8 / @@progress_steps
      sleep 2
      return false if $aborting || !@@flashing
      log "Flashing additional files post-rom..."
      flash_post(state)
      sleep 2
      return false if $aborting || !@@flashing
      log "Performing another systest..."
      TWRP.get_log
      flash_systest(state, "systest-after.log")
      Gui.do :progressbar_set, 9 / @@progress_steps
      sleep 2
      log "Done."
      $flash_progress[:rest] = true
    end
    # finish off the flashing process:
    flash_conductor
  end

  def flash_pre(state = nil)
    state = $device.state if state.nil?
    if state == "recovery" || $device.wait_for_recovery
      if @@files[:flashme_file_paths] && @@files[:flashme_file_paths][:pre].is_a?(Array)
        @@files[:flashme_file_paths][:pre].each do |file|
          log "Flashing #{file}..."
          if File.exist?(file)
            Gui.do :update_progress_title, "Flashing #{File.basename(file)}"
            $device.flash_zip(file)
            sleep 5
          else
            log "File #{file} does not exist."
          end
        end # each
      end # if
      $flash_progress[:flashme_pre] = true
    end
  end

  def flash_rom(state = nil)
    $reporter.report progress: "rom"
    state = $device.state if state.nil?
    if state == "recovery" || $device.wait_for_recovery
      # Gui.do :update_instructions, "Congratulations!\n\nPlease wait while the\nsystem is being installed."
      if @@settings[:dirty_flash]
        wipe = "dirty"
        Gui.do :update_progress_title, "Installing rom/zip..."
      else
        wipe = "clean"
        Gui.do :update_progress_title, "Wiping data and installing rom..."
      end
      if $device.flash_rom(@@files[:rom_file_path], wipe)
        $flash_progress[:rom] = true
        log "Rom flashed successfully!"
      else
        log "Flashing rom failed. Aborting."
        @@flashing = false
        Gui.do :btn_menu_click, 1   # unlock the gui menu
      end
    else
      Gui.do :update_instructions, "FAILED.\n\nReboot your device\nand try again."
      log "FAILED. Reboot your device and try again."
      @@flashing = false
      Gui.do :btn_menu_click, 1   # unlock the gui menu
    end
  end

  def flash_root(state = nil)
    $reporter.report progress: "root"
    state = $device.state if state.nil?
    if @@files[:root_file_path]
      Gui.do :update_progress_title, "Installing root addon..."
      $device.flash_zip(@@files[:root_file_path])
      $flash_progress[:root] = TWRP.last_sideload_success?
      if $flash_progress[:root]
        log "Root addon flashed successfully!"
      else
        log "Flashing root addon failed."
      end
    else
      # skip this step
      $flash_progress[:root] = true
      log "Skipped flashing root addon."
    end
  end

  def flash_nanodroid(state = nil)
    $reporter.report progress: "nanodroid"
    state = $device.state if state.nil?
    Gui.do :update_progress_title, "Installing NanoDroid..."
    read_settings_from_gui # again in case the user changed his mind in between
    setup = {microg: (@@settings[:microg] ? 1 : 0),
            fdroid: (@@settings[:fdroid] ? 1 : 0),
            play: (@@settings[:aurora] ? 31 : 01),
            swipe: (@@settings[:swype_libs] ? 1 : 0),
            gsync: (@@settings[:gsync] ? 1 : 0)}
    $device.flash_nanodroid(@@files[:nanodroid_file_path], setup)
    if TWRP.last_sideload_success?
      $flash_progress[:nanodroid] = true
    elsif TWRP.nanodroid_missing_space?
      log "Not enough space available in TWRP to flash the full NanoDroid package."
      Gui.do :update_progress_title, "Downloading and installing single NanoDroid packages..."
      log "Downloading and flashing single F-Droid and microG packages instead:"
      alt = Get.dl_nanodroid "", main: false, bromite: false, patcher: false, microg: true, fdroid: true
      $device.flash_zip alt[:fdroid] if alt[:fdroid]
      fdroid_success = TWRP.last_sideload_success?
      log "Flashing F-Droid successful: #{fdroid_success}"
      $device.flash_zip alt[:microg] if alt[:microg]
      microg_success = TWRP.last_sideload_success?
      log "Flashing microG successful: #{microg_success}"
      $flash_progress[:nanodroid] = fdroid_success && microg_success ? true : false
    else
      $flash_progress[:nanodroid] = false
    end
    if $flash_progress[:nanodroid]
      log "NanoDroid flashed successfully!"
    else
      log "Flashing NanoDroid failed."
    end
  end

  def flash_patcher(state = nil)
    $reporter.report progress: "patcher"
    state = $device.state if state.nil?
    if @@settings[:sigspoof]
      Gui.do :update_progress_title, "Patching system... (might take a while)"
      $device.flash_zip(@@files[:nanodroid_patcher_file_path])
      $flash_progress[:patcher] = TWRP.patcher_no_work || TWRP.last_sideload_success?
      if $flash_progress[:patcher]
        log "Patcher flashed successfully!"
      else
        log "Flashing patcher failed."
      end
    else
      # skip this step
      $flash_progress[:patcher] = true
      log "Skipped flashing patcher."
    end
  end

  def flash_post(state = nil)
    state = $device.state if state.nil?
    if state == "recovery" || $device.wait_for_recovery
      if @@files[:flashme_file_paths] && @@files[:flashme_file_paths][:post].is_a?(Array)
        @@files[:flashme_file_paths][:post].each do |file|
          log "Flashing #{file}..."
          if File.exist?(file)
            Gui.do :update_progress_title, "Flashing #{File.basename(file)}"
            $device.flash_zip(file)
            sleep 5
          else
            log "File #{file} does not exist."
          end
        end # each
      end # if
      $flash_progress[:flashme_post] = true
    end
  end

  def flash_systest(state = nil, save_to_file = nil)
    state = $device.state if state.nil?
    Gui.do :update_progress_title, "Gathering system information..."
    if state == "recovery" || $device.wait_for_recovery
      systest_file = $device.twrp.systest save_to_file
      if systest_file && File.exist?(systest_file)
        log "Successfully received systest info!"
      else
        log "Receiving systest info failed."
      end
    end
  end

  # called when main window is closed
  at_exit do
    ADB.kill_server
    Log.add "Killing the ADB server and closing the application."
  end

end
end # class Main
