#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module to handle fastboot shell commands

require_relative "os"
require_relative "adb"
require_relative "device"
require_relative "execute"
include Execute

module Fastboot
extend Log  # make log() method available as shortcut for Log.add()
class << self

  def devices
    # `#{fastboot_command} devices`.split(" ").first
    execute "#{fastboot_command} devices"
  end

  def connected?
    devices.strip != "" ? true : false
  end

  def reboot(target = "android")
    if target.downcase == "bootloader" || target.downcase == "fastboot"
      execute "#{fastboot_command} reboot bootloader"
    else
      execute "#{fastboot_command} reboot"
    end
  end

  def get_unlock_data
    log_on_fail do
      brand = $device.brand.downcase
      raw = send "get_unlock_data_#{brand}"
      send "scrub_raw_unlock_data_#{brand}", raw
    end
  end

  def get_unlock_data_motorola
    data = execute "#{fastboot_command} oem get_unlock_data", "ALL"
    log "---"
    log "command: fastboot oem get_unlock_data"
    log "Fastboot stdout:"
    log data[0]
    log "Fastboot stderr:"
    log data[1]
    log "---"
    data[0].empty? ? data[1] : data[0]
  end

  def get_unlock_data_sony
    ADB.connected? ? ADB.imei : nil
  end

  def scrub_raw_unlock_data_sony(raw)
    raw
  end

  def scrub_raw_unlock_data_motorola(raw)
    result = ""
    if raw.lines.first.include? "(bootloader) "
      raw.lines.each do |l|
        unless l.include?("slot") || l.include?("not found") || l.include?("nlock")
          result = result + l.split[-1].strip if l.include? "(bootloader) "
        end
      end
    elsif raw.lines.first.start_with? "INFO"
      raw.lines.each do |l|
        unless l.include?("slot") || l.include?("not found") || l.include?("nlock")
          result = result + l.split("INFO")[-1].strip if l.include? "INFO"
        end
      end
    else
      log "Could not parse unlock data from fastboot..."
      result = nil
    end
    result
  end

  def unlock(unlock_code)
    return false unless wait_for_fastboot
    send "unlock_#{$device.brand.downcase}", unlock_code
  end

  def unlock_motorola(unlock_code)
    response = execute "#{fastboot_command} oem unlock #{unlock_code}", "ALL"
    log "---"
    log "command: fastboot oem unlock #{unlock_code}"
    log "Fastboot stdout:"
    log response[0]
    log "Fastboot stderr:"
    log response[1]
    log "---"
    result = response[0].empty? ? response[1] : response[0]
    if result.downcase.include? "allow oem unlock"
      log "OEM unlock has apparently not been enabled..."
      Gui.do :alert, "It looks like you forgot to enable \"OEM unlock\" in the developer settings of your device.\nPlease reboot your device, enable it and try again."
      false
    elsif result.downcase.include? "re-run this command"
      log "Re-running the unlock command to confirm unlock request..."
      unlock_motorola unlock_code
    elsif result.downcase.include? "failed"
      log "Bootloader unlock failed with following message:"
      log result
      false
    elsif result.downcase.include?("already unlocked")
      log "Bootloader already unlocked. Trying to go directly to the next step..."
      Gui.do :chk_skip_bootloader_unlock_tick
      "already unlocked"
    elsif result.downcase.include?("is unlocked") || result.downcase.include?("succe") ||
          result.downcase.include?("okay") || result.downcase.include?("complete") ||
          result.downcase.include?("already unlocked")
      log "Bootloader unlocked successfully"
      Gui.do :chk_skip_bootloader_unlock_tick
      true
    else
      log "Unknown fastboot response:"
      log result
      false
    end
  end

  def flash_startup_logo(logo_file = nil)
    logo_file = Get.startup_logo $device.codename if logo_file.nil?
    return nil unless logo_file && File.exist?(logo_file)
    sleep 2
    response = execute "#{fastboot_command} flash logo #{logo_file}", "ALL"
    log "---"
    log "command: fastboot flash logo #{logo_file}"
    log "Fastboot stdout:"
    log response[0]
    log "Fastboot stderr:"
    log response[1]
    log "---"
    result = response[0].empty? ? response[1] : response[0]
    if result.downcase.include?("succe") || result.downcase.include?("okay") ||
      result.downcase.include?("done") || result.downcase.include?("complete")
      log "Successfully replaced startup logo."
      true
    else
      log "Failed to replace startup logo."
      false
    end
  end

  def unlock_sony(unlock_code)
    response = execute "#{fastboot_command} oem unlock 0x#{unlock_code}", "ALL"
    log "---"
    log "command: fastboot oem unlock 0x#{unlock_code}"
    log "Fastboot stdout:"
    log response[0]
    log "Fastboot stderr:"
    log response[1]
    log "---"
    result = response[0].empty? ? response[1] : response[0]
    if result.downcase.include? "not allowed"
      log "OEM unlock has apparently not been enabled..."
      Gui.do :alert, "It looks like you forgot to enable \"OEM unlock\" in the developer settings of your device.\nPlease reboot your device, enable it and try again."
      false
    elsif result.downcase.include? "already"
      log "Bootloader has already been unlocked. Continuing..."
      log result
      true
    elsif result.downcase.include? "failed"
      log "Bootloader unlock failed with following message:"
      log result
      false
    elsif result.downcase.include? "re-run this command"
      log "Re-running the unlock command to confirm unlock request..."
      unlock_sony unlock_code
    elsif result.downcase.include?("is unlocked") || result.downcase.include?("succe") || result.downcase.include?("okay")
      log "Bootloader unlocked successfully"
      true
    else
      log "Unknown fastboot response:"
      log result
      false
    end
  end

  def unlock_oneplus(unlock_code = nil)
    unlock_generic
  end

  def unlock_nvidia(unlock_code = nil)
    unlock_generic
  end

  def unlock_generic(unlock_code = nil)
    response = execute "#{fastboot_command} oem unlock", "ALL"
    log "---"
    log "command: fastboot oem unlock"
    log "Fastboot stdout:"
    log response[0]
    log "Fastboot stderr:"
    log response[1]
    log "---"
    result = response[0].empty? ? response[1] : response[0]
    if result.downcase.include? "allow oem unlock"
      log "OEM unlock has apparently not been enabled..."
      Gui.do :alert, "It looks like you forgot to enable \"OEM unlock\" in the developer settings of your device.\nPlease, reboot your device, enable it and try again."
      false
    elsif result.downcase.include? "failed"
      log "Bootloader unlock failed with following message:"
      log result
      false
    elsif result.downcase.include?("Total time: 0.000s")
      log "Bootloader already unlocked. Trying to go directly to the next step..."
      Gui.do :chk_skip_bootloader_unlock_tick
      "already unlocked"
    elsif result.downcase.include? "re-run this command"
      log "Re-running the unlock command to confirm unlock request..."
      unlock_generic
    elsif result.downcase.include?("is unlocked") || result.downcase.include?("succe") || result.downcase.include?("okay")
      log "Bootloader unlocked successfully"
      true
    else
      log "Unknown fastboot response:"
      log result
      false
    end
  end

  def flash_recovery(img_file, partition = nil)
    return false unless wait_for_fastboot
    if partition
      send "flash_recovery_#{$device.brand.downcase}", img_file, partition
    else
      send "flash_recovery_#{$device.brand.downcase}", img_file
    end
  end

  def flash_recovery_motorola(img_file, partition = "recovery")
    flash_recovery_generic img_file, partition
  end

  def flash_recovery_oneplus(img_file, partition = "recovery")
    flash_recovery_generic img_file, partition
  end

  def flash_recovery_nvidia(img_file, partition = "recovery")
    flash_recovery_generic img_file, partition
    # we might need to do "fastboot reboot-bootloader" before being able to boot to recovery
  end

  def flash_recovery_sony(img_file, partition = "boot")
    flash_recovery_generic img_file, partition
  end

  def flash_recovery_generic(img_file, partition = "recovery")
    unless partition.downcase == "boot"
      lookedup = Find.recovery_partition_name($device.codename || "")
      partition = lookedup if partition.nil? || lookedup != partition
      partition = "recovery" if partition.nil?
    end
    if partition.downcase == "boot"
      Gui.do :update_instructions, "Booting TWRP..."
      result_boot = execute "#{fastboot_command} boot #{img_file}", "ALL"
      log "---"
      log "command: fastboot boot #{img_file}"
      log "Fastboot stdout:"
      log result_boot[0]
      log "Fastboot stderr:"
      log result_boot[1]
      log "---"
      $force_twrp_update = true
      log "Will try to force permanent installation of TWRP..."
      sleep 3
      if TWRP.wait_for_me
        true
      else
        log "Failed to boot TWRP directly from fastboot."
        false
      end
    else
      result_flash = execute "#{fastboot_command} flash #{partition} #{img_file}", "ALL"
      # log result_flash unless result_flash[2].success?
      log "---"
      log "command: fastboot flash #{partition} #{img_file}"
      log "Fastboot stdout:"
      log result_flash[0]
      log "Fastboot stderr:"
      log result_flash[1]
      log "---"
      if result_flash[1].downcase.include?("no such partition") || result_flash[1].downcase.include?("invalid partition")
        log "Fastboot does not seem to know the partition name. Trying to boot TWRP directly..."
        flash_recovery_generic(img_file, partition = "boot")
      else
        result_flash[2].success?
      end
    end
  end

  # def release
  #   `#{fastboot_command} getprop ro.build.version.release`.strip
  # end
  #
  # def major
  #   release.chomp.split('.').first.to_i
  # end
  #
  # def serial
  #   `#{fastboot_command} getprop ro.serialno`.strip
  # end
  #
  # def device_name
  #   `#{fastboot_command} getprop ro.product.model`.strip
  # end

  def wait_for_me(max = 20)
    # Try to reboot into fastboot mode if adb is available
    if !connected? && ADB.connected?
      ADB.reboot "bootloader"
    end
    max.times do
      return true if connected?
      sleep 1
    end
    false
  end

  alias wait_for_fastboot wait_for_me

  def bin_version
    unless defined?($fastboot_bin_version) && defined?($fastboot_bin_path)
      version = execute "#{fastboot_command} --version"
      $fastboot_bin_version, $fastboot_bin_path = version.lines[0].split(" ")[-1], version.lines[1].split(" ")[-1]
    end
    $fastboot_bin_version
  end

  def bin_path
    unless defined?($fastboot_bin_version) && defined?($fastboot_bin_path)
      version = execute "#{fastboot_command} --version"
      $fastboot_bin_version, $fastboot_bin_path = version.lines[0].split(" ")[-1], version.lines[1].split(" ")[-1]
    end
    $fastboot_bin_path
  end

  private

  def fastboot_command
    case OS.which
    when "windows" then "bin/fastboot.exe"
    when "mac" then "bin/fastboot"
    else "sudo bin/fastboot"
    end
  end

end
end
