#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Class for managing usage reports to matomo server

class Reporter

  include Log

  def initialize
    # set to 0 after first report
    # this way we can see if free-droid had to be restarted
    @new_visit = 1
  end

  def report(params = {})
    # set default arguments for report parameters
    params = {action: nil, name: nil, value: nil, category: $device.codename}.merge(params)
    # skip reporting if we are in developer mode
    log_on_fail(quit: false) do
      unless $chk_tracking_consent_state || params[:force]
        puts "Skipped reporting: #{params}"
        return false
      end
      report = "idsite=3&rec=1&send_image=0"
      report = "#{report}&_id=#{$session["id"]}"
      report = "#{report}&new_visit=#{@new_visit}"
      @new_visit = 0
      report = "#{report}&ua=#{user_agent} Firefox/#{VERSION}"
      report = "#{report}&_cvar={\"1\":[\"Version\",\"#{VERSION}\"],\"2\":[\"Build\",\"#{BUILD}\"]}"
      report = "#{report}&e_c=#{params[:category]}"
      report = "#{report}&e_a=#{params[:action]}" if params[:action]
      report = "#{report}&e_n=#{params[:name]}" if params[:name]
      report = "#{report}&e_v=#{params[:value]}" if params[:value].is_a?(Integer) || params[:value].is_a?(Float)
      report = "#{report}&url=https://app/progress/#{params[:progress]}" if params[:progress]
      report = "#{report}&url=https://app/tracking/#{params[:tracking]}" if params[:tracking]
      # if more parameters given, add them to the report with their respective values:
      params.except!(:force, :_id, :new_visit, :ua, :_cvar, :progress, :action, :name, :value, :category, :e_a, :e_n, :e_v, :e_c)
      params.each { |k,v| report = "#{report}&#{k}=#{v}" }
      Get.retry_on_http_connection_problem do
        open("https://stats.free-droid.com/piwik.php?#{report}").status[0] == "204"
      end
    end
  end

  def send_bug_report
    Log.submit
  end

  def user_agent
    case OS.which
    when "linux" then "(Linux)"
    when "mac" then "(Macintosh)"
    when "windows" then "(Windows)"
    else "unknown"
    end
  end

end

# example report url
# https://matomo.amaury.cc/piwik.php?idsite=3&rec=1&send_image=0&e_c=testdevice&e_a=success&e_n=eventname

# add except method to the Hash class (used above)
class Hash
  def except(*keys)
    dup.except!(*keys)
  end
  def except!(*keys)
    keys.each { |key| delete(key) }
    self
  end
end
