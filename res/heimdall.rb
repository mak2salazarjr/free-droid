#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Module to handle heimdall shell commands

require_relative "os"
require_relative "adb"
require_relative "device"
require_relative "execute"
include Execute

module Heimdall
extend Log  # make log() method available as shortcut for Log.add()
class << self

  def detect
    execute "#{heimdall_command} detect"
  end

  def connected?
    detect != "" ? true : false
  end

  def flash_recovery_generic(img_file, partition = nil)
    # First, try and find out the recovery partition name if not provided
    # ...actually, just try partition name "RECOVERY" instead.
    # retry_on_connection_loss 2 do
    #   partition = find_recovery_partition_name
    # end if partition.nil?
    retry_on_connection_loss do
      # Try standard partition name of none found
      if partition
        flash_recovery_command(img_file, partition)
      else
        flash_recovery_command(img_file)
      end
    end
  end

  alias flash_recovery flash_recovery_generic

  def flash_recovery_command(img_file, partition_name = "RECOVERY")
    install_zadig_on_fail do
      result = execute "#{heimdall_command} flash --#{partition_name} #{img_file} --no-reboot", "ALL"
      log "---"
      log "Heimdall stdout:"
      log result[0]
      log "Heimdall stderr:"
      log result[1]
      log "---"
      result  # install_zadig_on_fail needs result returned from block
    end
  end

  def install_zadig_on_fail(try = 3, &block)
    try.times do |t|
      result = yield
      if result[2].success?
        return result
      else
        unless connected?
          wait_for_heimdall ? next : (return false)
        end
        if [result[0],result[1]].any? { |out| out.lines.last(5).any?{ |l| l.include?("Failed to access device")} }
          # launch zadig (if on windows)
          if OS.windows?
            Gui.do :alert, "Please install/replace the drivers for your device...\n\n
                Select from the list what could be your device and press the button.\n
                (Sometimes it can be names like 05c6:9008, SGH-T959V or Generic Serial.)\n\n
                You have #{try - t} tries left to find the correct one in the list."
            Get.zadig
            write_zadig_config unless File.exist?("bin/zadig.ini")
            Dir.chdir("bin") do
              execute 'Powershell -Command "& { Start-Process \"zadig.exe\" -Verb RunAs }'
            end
            Gui.do :ask_user_to_confirm_zadig_finished
            loop do
      				sleep 1
      				break if $user_confirmed_zadig_finished
      			end
            $user_confirmed_zadig_finished = false
          end
        end
      end
    end
  end

  def download_pit(pit_file = "current.pit")
    retry_on_connection_loss do
      execute "#{heimdall_command} download-pit --output #{pit_file} --no-reboot"
      File.exist? pit_file ? true : false
    end
  end

  # Query the device for partitions (pit). If pit_file provided
  # as argument, convert pit_file to readable text.
  def print_pit(pit_file = nil)
    if pit_file.nil?
      retry_on_connection_loss do
         result = execute "#{heimdall_command} print-pit --no-reboot", "ALL"
         result[0] if result[2].success?
      end
    else
      retry_on_connection_loss do
        result = execute "#{heimdall_command} print-pit --file #{pit_file} --no-reboot", "ALL"
        result[0] if result[2].success?
      end
    end
  end

  alias get_pit print_pit

  # Giving a pit file is optional.
  def find_recovery_partition_name(pit_file = nil)
    pit_file.nil? ? pit = get_pit : pit = get_pit(pit_file)
    pit = pit.split "\n"
    pit.each_with_index do |line, i|
      return pit[i-1].split(" ")[-1] if line.include?("recovery")
    end
    return false
  end

  def wait_for_me(max = 20)
    # Try to reboot into download mode if adb is available
    if !connected? && ADB.connected?
      ADB.reboot "download"
    end
    max.times do
      return true if connected?
      sleep 1
    end
    false
  end

  alias wait_for_heimdall wait_for_me

  def reboot
    retry_on_connection_loss do
      return true if ADB.connected?
      execute "#{heimdall_command} print-pit", "STATUS"
    end
  end

  def write_zadig_config
    File.open("bin/zadig.ini", "w") do |file|
      file.puts "[general]"
      file.puts "advanced_mode = false"
      file.puts "exit_on_success = true"
      file.puts "log_level = 0"
      file.puts "  "
      file.puts "[device]"
      file.puts "list_all = true"
      file.puts "include_hubs = false"
      file.puts "trim_whitespaces = true"
      file.puts "  "
      file.puts "[driver]  "
      file.puts "default_driver = 0"
      file.puts "extract_only = false"
      file.puts "  "
      file.puts "[security]"
    end
  end

  def bin_version
    unless defined? $heimdall_bin_version
      $heimdall_bin_version = execute("#{heimdall_command} version").strip.tr("v", "")
    end
    $heimdall_bin_version
  end

  def bin_path
    nil # heimdall binary does not give this information by itself
  end

  private

  # temporarily bypassing retry_on_connection_loss for testing purposes:
  def retry_on_connection_loss(try = 3, &block)
    yield
  end

  # Retries the given block and helps getting back to download mode
  # def retry_on_connection_loss(try = 3, &block)
  #   calling_method = caller_locations(1,1)[0].label
  #   try.times do |t|
  #     # only try to wait for heimdall if we do not want to reboot anyway
  #     unless calling_method == "reboot"
  #       wait_for_heimdall
  #     else
  #       # wait for connection if no connection detected
  #       if ! connected? && ! ADB.connected?
  #         30.times do
  #           break if connected? || ADB.connected?
  #           sleep 1
  #         end
  #       end
  #     end
  #     result = yield
  #     return result if result
  #     unless t == try - 1
  #       log "Lost connection to your device."
  #       log "Try to reboot into download mode by holding VOL- + HOME + POWER."
  #       printf "Press Enter when your device is ready. "
  #       gets
  #       log "Retrying..."
  #     end
  #   end # try.times
  #   log "Unable to connect to your device."
  #   return false
  # end

  def heimdall_command
    case OS.which
    when "windows" then "bin/heimdall.exe"
    # On macOS heimdall needs libusb-1.0.dylib which will be in the
    # same folder as the heimdall binary.
    when "mac" then "DYLD_LIBRARY_PATH=bin bin/heimdall"
    # apparently no need for sudo to use heimdall on linux
    else "sudo bin/heimdall"
    end
  end

end
end
