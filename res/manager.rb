#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# class for managing ressources like twrp and rom files available
# locally or from download sources

class Manager

  include Log

  attr_reader :official, :archive, :twrp, :local, :override, :user_provided_file,
              :suggest, :files_to_use, :user_choice, :use_alt_name, :flashme

  attr_writer :user_provided_file, :user_choice, :use_alt_name

  $manager_scanning = false

  def initialize
    reset
  end

  def reset
    @official = {}
    @archive = {}
    @twrp = {official: nil, archive: nil}
    @local = {}
    @NanoDroid = {}
    @override = {TWRP: nil, rom: nil}
    @user_provided_file = {TWRP: nil, rom: nil}
    # what to get from which source:
    @suggest = {TWRP_source: nil, rom: nil, rom_source: nil}
    @user_choice = {rom: nil, rom_source: nil}
    # filenames of the twrp and rom files to use:
    @files_to_use = {TWRP: nil, rom: nil}
    @use_alt_name = {TWRP: nil, rom: nil}
    @flashme = {pre: [], post: []}
    true
  end

  # scan all sources for TWRP and roms and save to manager object properties
  # return true if both TWRP and a rom are available
  def scan(codename = nil)
    $manager_scanning = true
    codename = $device.codename if codename.nil?
    return false if codename == "unknown"
    Dir.mkdir(codename.to_s) unless Dir.exist?(codename.to_s)
    Dir.mkdir("NanoDroid") unless Dir.exist?("NanoDroid")
    available = Get.available(codename)
    archive_available = available[:Archive]
    @official = available.select { |k| Get.roms.include? k.to_sym }
    @flashme[:pre] = archive_available[:flashme_pre] if archive_available[:flashme_pre]
    @flashme[:post] = archive_available[:flashme_post] if archive_available[:flashme_post]
    # @archive = archive_available.select { |k,v| Get.roms.include? k.to_sym }
    @archive = archive_available.reject do |k|
      [:TWRP,:twrp,:flashme_pre,:flashme_post].include?(k) || k.to_s.include?("override")
    end
    @twrp = {official: available[:TWRP], archive: archive_available[:TWRP]}
    @local = Dir.entries(codename.to_s).reject{|e| [".",".."].include? e}
    @NanoDroid = Dir.entries("NanoDroid").reject{|e| [".",".."].include? e}
    @override = {TWRP: archive_available[:override_TWRP], rom: archive_available[:override_rom]}
    suggest_to_use # populates @suggest and @files_to_use
    $manager_scanning = false
    @files_to_use.values.all?
  end

  def suggest_to_use  # from online available sources (official or archive)
    # TWRP
    if @override[:TWRP]
      @files_to_use[:TWRP] = @override[:TWRP]
      @suggest[:TWRP_source] = :archive
    else
      if @twrp[:official]
        @files_to_use[:TWRP] = @twrp[:official]
        @suggest[:TWRP_source] = :official
      elsif @twrp[:archive]
        @files_to_use[:TWRP] = @twrp[:archive]
        @suggest[:TWRP_source] = :archive
      end
    end # TWRP
    # Rom
    if @override[:rom]
      @files_to_use[:rom] = @override[:rom]
      @suggest[:rom_source] = :archive
      # find rom name that @override[:rom] belongs to, so we can display it correctly
      if @archive.key(@override[:rom])
        @suggest[:rom] = @archive.key(@override[:rom])
      else
        @suggest[:rom] = :override_rom
      end
    else
      if @official.values.any?
        @suggest[:rom_source] = :official
        if @official[:LineageOS]
          @files_to_use[:rom] = @official[:LineageOS]
          @suggest[:rom] = :LineageOS
        elsif @official[:LineageOS_MicroG]
          @files_to_use[:rom] = @official[:LineageOS_MicroG]
          @suggest[:rom] = :LineageOS_MicroG
        else
          @files_to_use[:rom] = @official.select{|k,v| v}.first[1]
          @suggest[:rom] = @official.select{|k,v| v}.first[0]
        end
      elsif @archive.values.any?
        @suggest[:rom_source] = :archive
        if @archive[:LineageOS]
          @files_to_use[:rom] = @archive[:LineageOS]
          @suggest[:rom] = :LineageOS
        elsif @archive[:LineageOS_MicroG]
          @files_to_use[:rom] = @archive[:LineageOS_MicroG]
          @suggest[:rom] = :LineageOS_MicroG
        else
          @files_to_use[:rom] = @archive.select{|k,v| v}.first[1]
          @suggest[:rom] = @archive.select{|k,v| v}.first[0]
        end
      end
    end
    @files_to_use
  end

  def something_missing?
    # ! @files_to_use.values.all?
    if !$chk_reflash_twrp_state && $device.custom_rom?
      twrp = true
    else
      twrp = @user_provided_file[:TWRP] || @files_to_use[:TWRP]
    end
    rom = @user_provided_file[:rom] || @files_to_use[:rom]
    ! [twrp, rom].all?
  end

  # same as in get_files but not threaded (used when updating twrp from twrp)
  def get_twrp(codename = nil)
    codename = $device.scan[:codename] unless codename
    if @override[:TWRP]
      Get.dl_archive(codename, :TWRP, @override[:TWRP])
    else
      if @suggest[:TWRP_source] == :official
        Get.get :TWRP, codename
      else
        Get.dl_archive(codename, :TWRP)
      end
    end
  end

  # Download everything we need and return relative paths to the files.
  # Does not download again if a file is already present.
  def get_files(codename = nil, settings = {})
    codename = $device.scan[:codename] unless codename
    # set default arguments for settings hash
    # do not download twrp if already assumed installed
    settings = {TWRP: !$device.custom_rom?, rom: true, magisk: true, NanoDroid: true,
                use_own_rom: false, flashme: true}.merge(settings)
    settings.merge({magisk: false, NanoDroid: false}) if settings[:only_flash_my_zip]
    result = {}
    return {} if codename == "unknown" || codename.nil?
    Gui.do :update_progress_title, "Downloading files..."
    rom = @user_choice[:rom] ? @user_choice[:rom] : @suggest[:rom]
    rom_source = @user_choice[:rom_source] ? @user_choice[:rom_source] : @suggest[:rom_source]
    if settings[:TWRP] == true || settings[:reflash_twrp] == true
      if @user_provided_file[:TWRP]
        result[:twrp_file_path] = @user_provided_file[:TWRP]
      elsif @override[:TWRP]
        result[:twrp_file_path] = Thread.new { Get.dl_archive(codename, :TWRP, @override[:TWRP]) }
      else
        if @suggest[:TWRP_source] == :official
          result[:twrp_file_path] = Thread.new { Get.get :TWRP, @use_alt_name[:TWRP] || codename }
        else
          result[:twrp_file_path] = Thread.new { Get.dl_archive(codename, :TWRP) }
        end
      end
    end
    if settings[:rom] == true
      if @user_provided_file[:rom]
        result[:rom_file_path] = @user_provided_file[:rom]
      elsif @override[:rom]
        result[:rom_file_path] = Thread.new { Get.dl_archive(codename, @suggest[:rom], @override[:rom]) }
      else
        if rom_source == :official
          result[:rom_file_path] = Thread.new { Get.get rom.to_sym, @use_alt_name[rom.to_sym] || codename }
        elsif rom_source == :archive
          result[:rom_file_path] = Thread.new { Get.dl_archive(codename, rom) }
        elsif rom_source == :own_rom
          result[:rom_file_path] = $own_rom_path
        end
      end
    end
    if settings[:magisk] == true
      result[:root_file_path] = Thread.new { Get.dl_magisk }
    else
      # needs to be done after we got systest info during flashing procedure...
      # if rom.to_s.downcase.start_with? "lineageos"
      #   result[:root_file_path] = Thread.new { Get.dl_lineageos_root_addon }
      # end
    end
    if settings[:flashme] && (@flashme[:pre][0] || @flashme[:post][0])
      result[:flashme_file_paths] = Thread.new { Get.flashme(codename, @flashme) }
    end
    if settings[:NanoDroid] == true      # no thread because of return value
      if settings[:NanoDroid_Bromite] == true
        @NanoDroid = Get.dl_nanodroid codename, {bromite: true}
        result[:nanodroid_bromite_file_path] = @NanoDroid[:bromite]
      else
        @NanoDroid = Get.dl_nanodroid codename, {bromite: false}
      end
      result[:nanodroid_file_path] = @NanoDroid[:main]
      result[:nanodroid_patcher_file_path] = @NanoDroid[:patcher]
    end
    Gui.do :progressbar_set, 1
    result.map{|k,v| [k, v.is_a?(Thread) ? v.join.value : v]}.to_h
  end

end
