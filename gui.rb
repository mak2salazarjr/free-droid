#     Free-Droid - Helping to flash custom roms onto Android devices.
#     Copyright (C) 2019  Amaury Bodet
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

require_relative "res/os"
require_relative "res/log"
extend Log

# Check for correct java version (1.7 or 1.8 needed)
running_java_version = Gem::Version.new java.lang.System.get_property('java.version').split("_")[0]
unless Gem::Version.new("1.7.0") <= running_java_version && running_java_version < Gem::Version.new("1.9.0")
  log "Please run this application with Java 7 or 8."
  exit!
end

# Under linux: do not launch with sudo, just provide sudo password when asked
if OS.linux? && `whoami`.strip == "root"
  log "Please do not launch this script with sudo, only provide the sudo password when asked."
  exit!
end

require "shoes"

# Define VERSION and BUILD
load "res/version.rb"

# Needed to call gui-methods from other scopes/threads/namespaces
$queue = Queue.new
module Gui
  def self.do(*args)
    $queue << args
  end
end

Shoes.app(title: "Free-Droid", width: 700, height: 300,
          resizable: false, border: true, scroll: false) do
  extend Log  # make Log methods available in here

  background lightgrey
  start do  # executes block when window is fully created
    Thread.new { log_on_fail { require_relative "res/main"; Main.setup } }
    animate(10) do
      until $queue.empty?
        work_unit = $queue.pop(true) rescue nil
        if work_unit[0].nil? || work_unit[0].empty?
          send nil rescue nil # don't ask why this is necessary...
        elsif work_unit.is_a?(Array)
          send(work_unit[0], *work_unit[1..-1])
        end
      end
    end
  end

  # use this for debug purposes in pry
  def getprop(prop)
    puts eval(prop)
  end

  @fontsize = case OS.which
  when "windows" then 11
  when "mac" then 13
  else 12
  end

  def startup_done
    @startup.hide
    menubar_buttons_visible true
    btn_menu_click "start"
  end

  # Does not really work
  def setup_failure(message = "Oops, something went wrong... Retry?")
    if confirm(message)
      Thread.new { log_on_fail { Main.setup } }
    else # cancel
      exit
    end
  end

  def close_app(message = nil)
    execute "bin/adb kill-server" rescue nil  # no sudo here
    alert message.to_s if message
    # if ask("Send a bug report with the log files?")
    #   alert "Thank you!" if $reporter.send_bug_report
    # end
    exit!
  end

  def update_downloaded
    alert "Please wait a few seconds and restart this application."
    Updater.replace_files
  end

  def update_failed
    alert "Failed to update. Please download the latest version by yourself."
    exit!
  end

  # used to inform the program that zadig is done
  $user_confirmed_zadig_finished = false

  def ask_user_to_confirm_zadig_finished
    alert "IMPORTANT:\n\nPlease press OK when driver installation is finished."
    $user_confirmed_zadig_finished = true
  end

  def antivirus_confirm(message)
    antivirus = confirm(message) ? "yes" : "no"
    $user_idea = ask("Please help fix this bug and type what you think caused it:")
    $antivirus = antivirus
  end

  # used to ask the user for the bootloader unlock code
  $bootloader_unlock_code = nil

  def ask_for_bootloader_unlock_code(message_before = nil, target_website = nil)
    alert message_before if message_before
    OS.open_browser target_website if target_website
    window title: "Bootloader unlock code", width: 300, height: 120, resizable: false, scroll: false do
      background lightgrey

      para "Bootloader unlock code:", align: "center", width: 1.0, margin: 10, size: @fontsize
      flow width: 1.0, margin_left: 10, margin_right: 10 do @input = edit_line width: 1.0 end
      flow width: 1.0, margin: 10 do
        button "Cancel", width: 0.5, margin: 5 do
          $bootloader_unlock_code = false
          close
        end
        button "Continue", width: 0.5, margin: 5 do
          $bootloader_unlock_code = @input.text.strip
          close
        end
      end
    end
  end

  # generic proc for a checkbox
  @chk_generic_proc = Proc.new { |checked, box_size = 20|
    a = (box_size / 5.0).round
    b = box_size - a - 1
    if checked
      border black
      line(a,a,b,b)
      line(b,a,a,b)
    else
      border black
    end
  }

  ### Menubar ###

  def menubar_buttons_visible(visible)
    @menubar.clear { @menubar_buttons_visible_proc.call(visible) }
  end

  def btn_menu_click(which)
    which = "m#{which}" if which.is_a?(Integer)
    case which.to_s.downcase
    when "m1", "start"
      @btn_m1.clear { @btn_m1_activate_proc.call(true) }
      @btn_m2.clear { @btn_m2_activate_proc.call(false) }
      @btn_m3.clear { @btn_m3_activate_proc.call(false)}
      @btn_m4.clear { @btn_m4_activate_proc.call(false) }
      @btn_m5.clear { @btn_m5_activate_proc.call(false) }
    when "m2", "settings"
      @btn_m1.clear { @btn_m1_activate_proc.call(false) }
      @btn_m2.clear { @btn_m2_activate_proc.call(true) }
      @btn_m3.clear { @btn_m3_activate_proc.call(false) }
      @btn_m4.clear { @btn_m4_activate_proc.call(false) }
      @btn_m5.clear { @btn_m5_activate_proc.call(false) }
    when "m3", "expert"
      @btn_m1.clear { @btn_m1_activate_proc.call false }
      @btn_m2.clear { @btn_m2_activate_proc.call false }
      @btn_m3.clear { @btn_m3_activate_proc.call true }
      @btn_m4.clear { @btn_m4_activate_proc.call false }
      @btn_m5.clear { @btn_m5_activate_proc.call false }
    when "m4", "help"
      @btn_m1.clear { @btn_m1_activate_proc.call false }
      @btn_m2.clear { @btn_m2_activate_proc.call false }
      @btn_m3.clear { @btn_m3_activate_proc.call false }
      @btn_m4.clear { @btn_m4_activate_proc.call true }
      @btn_m5.clear { @btn_m5_activate_proc.call false }
    when "m5", "about"
      @btn_m1.clear { @btn_m1_activate_proc.call false }
      @btn_m2.clear { @btn_m2_activate_proc.call false }
      @btn_m3.clear { @btn_m3_activate_proc.call false }
      @btn_m4.clear { @btn_m4_activate_proc.call false }
      @btn_m5.clear { @btn_m5_activate_proc.call true }
    end
  end

  # Show or hide buttons

  @btn_m1_activate_proc = Proc.new { |active|
    color = active ? black : grey
    inside_btn_m1 = flow width: 1.0, height: 1.0 do
      border color
      para "START", width: 1.0, height: 1.0, align: "center", margin_top: 4, size: @fontsize
    end
    active ? @start.show : @start.hide
    inside_btn_m1.click do
      btn_menu_click "m1"
    end unless active
  }

  @btn_m2_activate_proc = Proc.new { |active|
    color = active ? black : grey
    inside_btn_m2 = flow width: 1.0, height: 1.0 do
      border color
      para "SETTINGS", width: 1.0, height: 1.0, align: "center", margin_top: 4, size: @fontsize
    end
    active ? @settings.show : @settings.hide
    inside_btn_m2.click do
      btn_menu_click "m2"
    end unless active
  }

  @btn_m3_activate_proc = Proc.new { |active|
    color = active ? black : grey
    inside_btn_m3 = flow width: 1.0, height: 1.0 do
      border color
      para "EXPERT", width: 1.0, height: 1.0, align: "center", margin_top: 4, size: @fontsize
    end
    active ? @expert.show : @expert.hide
    inside_btn_m3.click do
      btn_menu_click "m3"
    end unless active
  }

  @btn_m4_activate_proc = Proc.new { |active|
    color = active ? black : grey
    inside_btn_m4 = flow width: 1.0, height: 1.0 do
      border color
      para "HELP", width: 1.0, height: 1.0, align: "center", margin_top: 4,size: @fontsize
    end
    active ? @help.show : @help.hide
    inside_btn_m4.click do
      btn_menu_click "m4"
    end unless active
  }

  @btn_m5_activate_proc = Proc.new { |active|
    color = active ? black : grey
    inside_btn_m5 = flow width: 1.0, height: 1.0 do
      border color
      para "ABOUT", width: 1.0, height: 1.0, align: "center", margin_top: 4, size: @fontsize
    end
    active ? @about.show : @about.hide
    inside_btn_m5.click do
      btn_menu_click "m5"
    end unless active
  }

  # Locks menu buttons (greyed out and not clickable)
  # (Call "Gui.do :btn_menu_click, 1" to reneable the menu buttons)

  def lock_menu_buttons(btn = 1)
    btn_menu_click(btn)
    @btn_m1.clear { @btn_m1_disable_proc.call } unless btn == 1
    @btn_m2.clear { @btn_m2_disable_proc.call } unless btn == 2
    @btn_m3.clear { @btn_m3_disable_proc.call } unless btn == 3
    @btn_m4.clear { @btn_m4_disable_proc.call } unless btn == 4
    @btn_m5.clear { @btn_m5_disable_proc.call } unless btn == 5
  end

  @btn_m1_disable_proc = Proc.new {
    inside_btn_m1 = flow width: 1.0, height: 1.0 do
      border grey
      para "START", width: 1.0, height: 1.0, align: "center", margin_top: 4, stroke: grey, size: @fontsize
    end
  }
  @btn_m2_disable_proc = Proc.new {
    inside_btn_m2 = flow width: 1.0, height: 1.0 do
      border grey
      para "SETTINGS", width: 1.0, height: 1.0, align: "center", margin_top: 4, stroke: grey, size: @fontsize
    end
  }
  @btn_m3_disable_proc = Proc.new {
    inside_btn_m3 = flow width: 1.0, height: 1.0 do
      border grey
      para "EXPERT", width: 1.0, height: 1.0, align: "center", margin_top: 4, stroke: grey, size: @fontsize
    end
  }
  @btn_m4_disable_proc = Proc.new {
    inside_btn_m4 = flow width: 1.0, height: 1.0 do
      border grey
      para "HELP", width: 1.0, height: 1.0, align: "center", margin_top: 4, stroke: grey, size: @fontsize
    end
  }

  @btn_m5_disable_proc = Proc.new {
    inside_btn_m5 = flow width: 1.0, height: 1.0 do
      border grey
      para "ABOUT", width: 1.0, height: 1.0, align: "center", margin_top: 4, stroke: grey, size: @fontsize
    end
  }

  @menubar_buttons_visible_proc = Proc.new { |visible, placeholder_text = "  Welcome"|
    if visible
      @btn_m1 = flow width: 0.2, height: 1.0, margin_right: 5 do
        @btn_m1_activate_proc.call(true)
      end
      @btn_m2 = flow width: 0.2, height: 1.0, margin_left: 5, margin_right: 5 do
        @btn_m2_activate_proc.call(false)
      end
      @btn_m3 = flow width: 0.2, height: 1.0, margin_left: 5, margin_right: 5 do
        @btn_m3_activate_proc.call(false)
      end
      @btn_m4 = flow width: 0.2, height: 1.0, margin_left: 5, margin_right: 5 do
        @btn_m4_activate_proc.call(false)
      end
      @btn_m5 = flow width: 0.2, height: 1.0, margin_left: 5 do
        @btn_m5_activate_proc.call(false)
      end
    else
      para placeholder_text, align: "center", size: @fontsize + 3
    end
  }

  @menubar = flow width: 1.0, height: 0.14, margin_right: 20, margin_left: 20, margin_top: 15 do
    @menubar_buttons_visible_proc.call(false)
    # @btn_m1 = flow width: 0.2, height: 1.0, margin_right: 5 do
    #   @btn_m1_activate_proc.call(true)
    # end
    # @btn_m2 = flow width: 0.2, height: 1.0, margin_left: 5, margin_right: 5 do
    #   @btn_m2_activate_proc.call(false)
    # end
    # @btn_m3 = flow width: 0.2, height: 1.0, margin_left: 5, margin_right: 5 do
    #   @btn_m3_activate_proc.call(false)
    # end
    # @btn_m4 = flow width: 0.2, height: 1.0, margin_left: 5, margin_right: 5 do
    #   @btn_m4_activate_proc.call(false)
    # end
    # @btn_m5 = flow width: 0.2, height: 1.0, margin_left: 5 do
    #   @btn_m5_activate_proc.call(false)
    # end
  end


  ### Startup Panel ###

  def startup_internet_connection(connection)
    @startup_internet_connection.clear { @startup_internet_connection_proc.call(connection) }
  end

  @startup_internet_connection_proc = Proc.new { |connection|
    if connection
      para "Server reachable: YES", margin: 5, align: "center", size: @fontsize
    else
      para "Server reachable: NO", margin: 5, align: "center", size: @fontsize
    end
  }

  def startup_app_up_to_date(check)
    @startup_app_up_to_date.clear { @startup_app_up_to_date_proc.call(check) }
  end

  @startup_app_up_to_date_proc = Proc.new { |check|
    if check
      para "Application up to date: YES", margin: 5, align: "center", size: @fontsize
    else
      para "Application updating... Please wait.", margin: 5, align: "center", size: @fontsize
    end
  }

  def startup_got_binaries(check)
    @startup_got_binaries.clear { @startup_got_binaries_proc.call(check) }
  end

  @startup_got_binaries_proc = Proc.new { |check|
    if check
      para "Downloaded binaries: YES", margin: 5, align: "center", size: @fontsize
    else
      para "Downloading binaries... Please wait.", margin: 5, align: "center", size: @fontsize
    end
  }

  def startup_linux_sudo_notice
    @startup_linux_sudo.show
  end

  @startup = flow width: 1.0, height: 0.86, margin_right: 10, margin_left: 10, margin_bottom: 10 do
    flow width: 1.0, height: 1.0, margin: 10 do
      border grey
      stack width: 1.0, height: 0.9 do
        stack width: 1.0, height: 0.2 do
          @title_string = " Please wait while the application is preparing..."
          para @title_string, justify: true, align: "center", margin_left: 20, margin_right: 20, margin_top: 20, size: @fontsize + 2
        end
        stack width: 1.0, height: 0.8, margin: 20 do
          @startup_internet_connection = flow width: 1.0 do
            para "Server reachable: ?", margin: 5, align: "center", size: @fontsize
          end
          @startup_app_up_to_date = flow width: 1.0 do
            para "Application up to date: ?", margin: 5, align: "center", size: @fontsize
          end
          @startup_got_binaries = flow width: 1.0 do
            para "Downloaded binaries: ?", margin: 5, align: "center", size: @fontsize
          end
          @startup_linux_sudo = flow width: 1.0 do
            para "Linux users: launch from terminal and provide sudo password when asked", margin: 10, align: "center", justify: true, size: @fontsize
          end
          @startup_linux_sudo.hide
        end
        # stack width: 0.5, height: 0.8, margin: 10 do
        #
        # end
      end
      flow width: 1.0, height: 0.1 do
        @author_and_stuff_string = "Version: #{VERSION}  -  Build: #{BUILD}  -  License: GPL-3.0"
        @author_and_stuff_string = "#{@author_and_stuff_string}  -  Developer Mode" if ARGV.any?{ |arg| ["dev", "-dev", "--dev", "d", "-d"].include?(arg) }
        @author_and_stuff = para @author_and_stuff_string, align: "center", size: @fontsize - 2
        @author_and_stuff.click {OS.open_browser("https://free-droid.com")}
      end
    end
  end


  ### Start Panel ###

  $ready_to_flash = false

  @btn_start_activate_proc = Proc.new { |active|
    color = active ? mediumseagreen : orangered
    inside_btn_start = flow width: 1.0, height: 1.0 do
      border color, strokewidth: 2
      para "START", width: 1.0, height: 1.0, align: "center", margin_top: 13, size: @fontsize
    end
    inside_btn_start.click { log_on_fail { Main.flash } } if active
  }

  $btn_start_state = false

  def btn_start_update
    $chk_confirm_state && $ready_to_flash && !$manager_scanning ? btn_start_activate : btn_start_deactivate
  end

  def btn_start_activate
    $btn_start_state = true
    @btn_start.clear { @btn_start_activate_proc.call(true) }
  end

  def btn_start_deactivate
    $btn_start_state = false
    @btn_start.clear { @btn_start_activate_proc.call(false) }
  end

  def btn_start_toggle
    $btn_start_state ? btn_start_deactivate : btn_start_activate
  end

  @device_state_indicator_proc = Proc.new { |text, size = @fontsize, color = "black"|
    para text, align: "center", top: 5, size: size, stroke: eval(color)
  }

  def update_device_state_indicator(state, size = @fontsize, color = "black")
    @device_state_indicator.clear { @device_state_indicator_proc.call(state, size, color) }
  end

  @instructions_string = "On your connected Android device:\n1. In Settings > About Phone :\nTap 7 times on Build Number\n2. In Settings > Developer Options :\nactivate Android/USB Debugging\nand OEM Unlock (if you have that)"

  @instructions_proc = Proc.new { |new_instructions, size = @fontsize, margin_top = 33, margin_left = 30|
    border grey
    stack width: 1.0, margin_top: margin_top, margin_left: margin_left, margin_right: 10 do
      para new_instructions, justify: true, size: size
    end
  }

  def update_instructions(new_instructions = @instructions_string, size = @fontsize, margin_top = 33, margin_left = 30)
    return update_instructions(@instructions_string, 11, 27, 39) if new_instructions.empty?
    @instructions.clear { @instructions_proc.call(new_instructions, size, margin_top, margin_left) }
  end

  $listbox_candidates_items = []
  $listbox_candidates_selected = nil

  @identify_device_proc = Proc.new { |candidates, size = @fontsize, margin_top = 20, margin_left = 30|
    border grey
    stack width: 1.0, margin_top: margin_top, margin_left: margin_left, margin_right: margin_left do
      flow width: 1.0 do
        para "Please select your device:", justify: true, size: size
        @listbox_candidates = list_box items: $listbox_candidates_items, width: 0.85, margin_top: 5
        flow width: 0.15, height: @listbox_candidates.height, margin: 5 do
          @img_reload_candidates = image "res/reload.png", height: 1.0, right: 0, top: 5
          @img_reload_candidates.click do
            Thread.new {
              Find.possible_candidates($device.name)
              Gui.do :identify_device_show  # update listbox items
            }
          end
        end
      end
      @listbox_candidates.change do
        # Make selection attribute publicly readable
        $listbox_candidates_selected = @listbox_candidates.text
      end
      @btn_identify = flow width: 1.0, height: 50, margin_top: 20, margin_left: margin_left, margin_right: margin_left do
        @btn_identify_inner = flow width: 1.0, height: 1.0 do
          border green, strokewidth: 2
          para "Confirm", width: 1.0, height: 1.0, align: "center", margin_top: 6, size: @fontsize
        end
      end
      @btn_identify_inner.click do
        Gui.do :identify_device_hide
        Gui.do :update_instructions, "Please wait..."
          unless @listbox_candidates.items.empty? || $listbox_candidates_selected.nil? || $listbox_candidates_selected == ""
            Thread.new {
              $device.device_identified_by_user = $listbox_candidates_selected
              $device.scan
              $device.notifier.update($device, $device.state ? $device.state : "android")
            }
          else
            Gui.do :alert, "Unable to identify your device, sorry."
          end
      end
    end
  }

  def identify_device_show
    @listbox_candidates.items = $listbox_candidates_items
    @instructions.hide
    @identify_device.show
  end

  def identify_device_hide
    @identify_device.hide
    @instructions.show
  end

  $chk_confirm_state = false

  @chk_confirm_proc = Proc.new { |checked|
    if checked
      border mediumseagreen
      line(4,4,25,25)
      line(25,4,4,25)
    else
      border orangered
    end
  }

  def chk_confirm_toggle
    if $chk_confirm_state
      $chk_confirm_state = false
      @chk_confirm.clear { @chk_confirm_proc.call(false) }
    else
      $chk_confirm_state = true
      @chk_confirm.clear { @chk_confirm_proc.call(true) }
    end
    btn_start_update
  end

  $first_connection = false

  def first_connection
    @initial_help.hide
    @progressbox.show
    update_device_state_indicator ""
  end

  @progress_title_proc = Proc.new { |title|
    para title.to_s, align: "center", size: @fontsize
  }

  @current_progress_title = ""

  def update_progress_title(title)
    @progress_title.clear { @progress_title_proc.call(title) }
    @current_progress_title = title
  end

  def reset
    btn_menu_click 1   # unlock the gui menu
    btn_abort_visible false
    btn_start_deactivate
    reset_progressbar
    $device.notifier.reset_rom_choice_gui
    chk_magisk_tick
    chk_microg_tick
    chk_fdroid_tick
    chk_aurora_tick
    chk_only_flash_my_zip_untick
    chk_sigspoof_tick
    chk_swype_libs_untick
    chk_gsync_adapters_untick
    chk_skip_bootloader_unlock_untick
    chk_reflash_twrp_untick
    chk_use_own_twrp_untick
    chk_dirty_flash_untick
  end

  def reset_progressbar(new_title = nil)
    progressbar_set 0
    if new_title
      update_progress_title new_title.to_s
    else
      @current_progress_title = "#{@current_progress_title}." unless @current_progress_title.end_with?(".")
      update_progress_title "#{@current_progress_title} Done."
    end
  end

  def progressbar_get
    @progressbar.fraction
  end

  def progressbar_set(progress)
    @progressbar.fraction = progress
  end

  @view_video_instructions_link_proc = Proc.new { |hover|
    text = "Click here to view video instructions"
    if hover
      # border slateblue
      para text, align: "center", top: 10, stroke: blue, underline: "single", size: @fontsize
    else
      # border grey
      para text, align: "center", top: 10, size: @fontsize
    end
  }

  @driver_install_link_proc = Proc.new { |hover|
    text = "Still not detecting your device?"
    if hover
      # border slateblue
      para text, align: "center", top: 10, stroke: blue, underline: "single", size: @fontsize
    else
      # border grey
      para text, align: "center", top: 10, size: @fontsize
    end
  }

  @initial_help_proc = Proc.new { |hover|
    border grey
    @view_video_instructions_link = stack width: (OS.windows? ? 0.49 : 1.0) do
      @view_video_instructions_link_proc.call(false)
    end
    @view_video_instructions_link.click {
      ask_youtube_how_to_enable_android_debugging
    }
    @view_video_instructions_link.hover { @view_video_instructions_link.clear {@view_video_instructions_link_proc.call(true)} }
    @view_video_instructions_link.leave { @view_video_instructions_link.clear {@view_video_instructions_link_proc.call(false)} }
    if OS.windows?
      @driver_install_link = stack width: 0.49 do
        @driver_install_link_proc.call(false)
      end
      @driver_install_link.click {
        Thread.new { log_on_fail { Main.install_adb_driver } }
        alert "Ok, let's try to install drivers.\n\nPlease click Install in the little program that will open in a few seconds.\n\nIf nothing happens after you click OK, launch universaladbdriver_v6.0.exe\nin the Free-Droid folder."
      }
      @driver_install_link.hover { @driver_install_link.clear {@driver_install_link_proc.call(true)} }
      @driver_install_link.leave { @driver_install_link.clear {@driver_install_link_proc.call(false)} }
    end
  }

  def btn_abort_visible(value)
    value ? @btn_abort.show : @btn_abort.hide
  end

  @start = flow width: 1.0, height: 0.86, margin_right: 10, margin_left: 10, margin_bottom: 10 do
    stack width: 0.5, height: 0.8, margin: 10 do
      border grey
      @btn_start = flow width: 1.0, height: 82, margin: 20 do
        @btn_start_activate_proc.call(false)
      end
      flow width: 1.0, margin_left: 20 do
        @chk_confirm = flow width: 30, height: 30 do
          @chk_confirm_proc.call(false)
        end
        @chk_confirm.click { chk_confirm_toggle }
        para "  I have backups of what I need", margin: 5, size: @fontsize
      end
      flow width: 1.0, margin_left: 20, margin_top: 20 do
        @device_state_indicator = flow width: 1.0, height: 30 do
          @device_state_indicator_proc.call("No device detected", 13)
        end
      end
      @btn_abort = flow width: 1.0, height: 50, margin_left: 110, margin_right: 110, margin_top: 20 do
        inside_btn_abort = flow width: 1.0, height: 1.0 do
          border darkorange, strokewidth: 2
          para "ABORT", width: 1.0, height: 1.0, align: "center", margin_top: 7, size: @fontsize - 1
        end
        inside_btn_abort.click { log_on_fail { Main.abort } }
      end.hide
    end
    @instructions = stack width: 0.5, height: 0.8, margin: 10 do
      @instructions_proc.call(@instructions_string, 11, 27, 39)
    end
    @identify_device = stack width: 0.5, height: 0.8, margin: 10 do
      @identify_device_proc.call
    end.hide
    @progressbox = flow width: 1.0, height: 0.2, margin_bottom: 10, margin_left: 10, margin_right: 10 do
      # border grey
      flow width: 1.0, height: 1.0 do
        @progress_title = stack width: 1.0, height: 0.5 do
          # @progress_title_proc.call("")
          update_progress_title ""
        end
        stack width: 1.0, height: 0.5 do
          @progressbar = progress width: 1.0, height: 17, margin_left: 20, margin_right: 20
        end
      end
    end.hide
    @initial_help = flow width: 1.0, height: 0.2, margin_bottom: 10, margin_left: 10, margin_right: 10 do
      @initial_help_proc.call(false)
    end
    # @initial_help.click {
    #   ask_youtube_how_to_enable_android_debugging
    # }
    # @initial_help.hover { @initial_help.clear {@initial_help_proc.call(true)} }
    # @initial_help.leave { @initial_help.clear {@initial_help_proc.call(false)} }
  end
  @start.hide


  ### Settings Panel ###

  $chk_archive_state = false
  $chk_archive_active = false

  @chk_archive_proc = Proc.new { |checked|
    color = $chk_archive_active ? black : grey
    if checked
      inside = flow width: 1.0, height: 1.0 do
        border color
        line(4,4,15,15)
        line(15,4,4,15)
      end
    else
      inside = flow width: 1.0, height: 1.0 do
        border color
      end
    end
    inside.click do
      chk_archive_toggle
    end if $chk_archive_active
  }

  def chk_archive_active(state = false)
    if state
      $chk_archive_active = true
      @chk_archive.clear { @chk_archive_proc.call($chk_archive_state) }
      @chk_archive_label.style(stroke: black)
    else
      $chk_archive_active = false
      @chk_archive.clear { @chk_archive_proc.call($chk_archive_state) }
      @chk_archive_label.style(stroke: grey)
    end
    $device.notifier.switch_rom_source
  end

  def chk_archive_activate
    chk_archive_active(true)
  end

  def chk_archive_deactivate
    chk_archive_active(false)
  end

  def chk_archive_toggle
    $chk_archive_state ? chk_archive_untick : chk_archive_tick
  end

  def chk_archive_tick
    unless $chk_archive_state
      $chk_archive_state = true
      @chk_archive.clear { @chk_archive_proc.call(true) }
      $device.notifier.switch_rom_source
    end
  end

  def chk_archive_untick
    if $chk_archive_state
      $chk_archive_state = false
      @chk_archive.clear { @chk_archive_proc.call(false) }
      $device.notifier.switch_rom_source
    end
  end

  $chk_magisk_state = true
  $chk_magisk_clicked = true # no automatic change

  def chk_magisk_tick
    $chk_magisk_state = true
    @chk_magisk.clear { @chk_generic_proc.call(true) }
  end

  def chk_magisk_untick
    $chk_magisk_state = false
    @chk_magisk.clear { @chk_generic_proc.call(false) }
  end

  # toggle magisk by click
  def chk_magisk_toggle
    $chk_magisk_clicked = true
    if $chk_magisk_state
      chk_magisk_untick
    else
      chk_magisk_tick
    end
  end

  $chk_fdroid_state = true

  def chk_fdroid_tick
    $chk_fdroid_state = true
    @chk_fdroid.clear { @chk_generic_proc.call(true) }
  end

  def chk_fdroid_untick
    $chk_fdroid_state = false
    @chk_fdroid.clear { @chk_generic_proc.call(false) }
  end

  # toggle fdroid by click
  def chk_fdroid_toggle
    if $chk_fdroid_state
      chk_fdroid_untick
    else
      chk_fdroid_tick
    end
  end

  $chk_microg_state = true

  def chk_microg_tick
    $chk_microg_state = true
    @chk_microg.clear { @chk_generic_proc.call(true) }
  end

  def chk_microg_untick
    $chk_microg_state = false
    @chk_microg.clear { @chk_generic_proc.call(false) }
  end

  # toggle microg by click
  def chk_microg_toggle
    if $chk_microg_state
      chk_microg_untick
    else
      chk_microg_tick
    end
  end

  $chk_aurora_state = true

  def chk_aurora_tick
    $chk_aurora_state = true
    @chk_aurora.clear { @chk_generic_proc.call(true) }
  end

  def chk_aurora_untick
    $chk_aurora_state = false
    @chk_aurora.clear { @chk_generic_proc.call(false) }
  end

  # toggle aurora by click
  def chk_aurora_toggle
    if $chk_aurora_state
      chk_aurora_untick
    else
      chk_aurora_tick
    end
  end

  $chk_use_own_rom_state = false
  $own_rom_path = nil

  def chk_use_own_rom_tick
    log_on_fail do
      file_path = ask_open_file
      if file_path && file_path.end_with?(".zip")
        $own_rom_path = file_path
        $chk_use_own_rom_state = true
        $manager.user_provided_file[:rom] = file_path
        @chk_use_own_rom.clear { @chk_generic_proc.call(true) }
        @listbox_roms.hide
        @img_reload.hide
        @chk_archive.hide
        @chk_archive_label.hide
        @android_version_label.hide
        $device.notifier.update_gui
      else
        $own_rom_path = nil
      end
    end
  end

  def chk_use_own_rom_untick
    $own_rom_path = nil
    $chk_use_own_rom_state = false
    $manager.user_provided_file[:rom] = nil
    @chk_use_own_rom.clear { @chk_generic_proc.call(false) }
    @listbox_roms.show
    @img_reload.show
    @chk_archive.show
    @chk_archive_label.show
    @android_version_label.show
  end

  # toggle use_own_rom by click
  def chk_use_own_rom_toggle
    if $chk_use_own_rom_state
      chk_use_own_rom_untick
    else
      chk_use_own_rom_tick
    end
  end

  $listbox_roms_items = []
  $listbox_roms_selected = nil

  def update_listbox_roms(items = $listbox_roms_items)
    @listbox_roms.items = items
    # listbox_roms_choose $listbox_roms_selected
  end

  def listbox_roms_choose(item)
    $listbox_roms_selected = item.to_s
    @listbox_roms.choose(item.to_s)
  end

  @android_version_label_proc = Proc.new { |version, name|
    para "Android #{version} #{name}".strip, align: "right", size: @fontsize - 3, margin_top: 4
  }

  def update_android_version_label(version = nil, name = nil)
    # First find the rom filename and parse the android version
    if version.nil? || name.nil?
      if $listbox_roms_selected
        source = $manager.user_choice[:rom_source]
        rom = $listbox_roms_selected.to_sym
        filename = $manager.send(source)[rom]
        version = Get.android_version(filename)
        name = Get.android_name(version)
      end
    end
    # Now update the label on the GUI
    if version.nil? && name.nil?
      @android_version_label.hide
    else
      version = "" if version.nil?
      name = "" if name.nil?
      @android_version_label.clear { @android_version_label_proc.call(version, name) }
      @android_version_label.show
    end
  end

  @choose_your_rom_label_proc = Proc.new { |text = "Choose your rom:"|
    para text, align: "left", size: @fontsize
  }

  def change_choose_your_rom_label(text = "Choose your rom:")
    @choose_your_rom_label.clear { @choose_your_rom_label_proc.call(text) }
  end

  @settings = flow width: 1.0, height: 0.86, margin_right: 10, margin_left: 10, margin_bottom: 10 do
    stack width: 0.5, height: 0.8, margin: 10 do
      border grey
      flow width: 1.0, margin_top: 25, margin_left: 14, margin_right: 14 do
        @choose_your_rom_label = stack width: 0.5 do
          @choose_your_rom_label_proc.call
        end
        @android_version_label = stack width: 0.5, height: 1.0 do
          @android_version_label_proc.call
        end.hide
      end
      stack width: 1.0, margin: 14 do
        flow width: 1.0 do
          @listbox_roms = list_box items: $listbox_roms_items, width: 0.85
          @listbox_roms.change do
            # Make selection attribute publicly readable
            $listbox_roms_selected = @listbox_roms.text
            $device.notifier.user_selected_a_rom
            update_android_version_label
          end
          flow width: 0.15, height: @listbox_roms.height, margin: 5 do
            @img_reload = image "res/reload.png", height: 1.0, right: 0
            @img_reload.click do
              Thread.new { $device.notifier.rescan_available_roms }
            end
          end
        end
      end
      flow width: 1.0, margin_left: 0.25 do
        @chk_archive = flow width: 20, height: 20 do
          @chk_archive_proc.call(false)
        end
        @chk_archive_label = para "  unofficial archive", top: 1, stroke: grey, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.25, margin_top: 18 do
        @chk_use_own_rom = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_use_own_rom_state)
        end
        @chk_use_own_rom.click { chk_use_own_rom_toggle }
        @chk_use_own_rom_label = para "  use a zip file", top: 1, size: @fontsize
      end
    end
    stack width: 0.5, height: 0.8, margin: 10 do
      border grey
      flow width: 1.0, margin_left: 0.2, margin_top: 24 do
        @chk_magisk = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_magisk_state)
        end
        @chk_magisk.click { chk_magisk_toggle }
        @chk_magisk_label = para "  install Magisk (root)", top: 1, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.2, margin_top: 18 do
        @chk_microg = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_microg_state)
        end
        @chk_microg.click { chk_microg_toggle }
        @chk_microg_label = para "  install microG", top: 1, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.2, margin_top: 18 do
        @chk_fdroid = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_fdroid_state)
        end
        @chk_fdroid.click { chk_fdroid_toggle }
        @chk_fdroid_label = para "  install F-Droid", top: 1, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.2, margin_top: 18 do
        @chk_aurora = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_aurora_state)
        end
        @chk_aurora.click { chk_aurora_toggle }
        @chk_aurora_label = para "  install Aurora Store", top: 1, size: @fontsize
      end
    end
    flow width: 1.0, height: 0.2, margin_bottom: 10, margin_left: 10, margin_right: 10 do
      border grey
      para "Only change these settings if you know what you are doing.", align: "center", top: 10, size: @fontsize
    end
  end
  @settings.hide


  ### Expert Panel ###

  $chk_only_flash_my_zip_state = false

  def chk_only_flash_my_zip_tick
    $chk_only_flash_my_zip_state = true
    @chk_only_flash_my_zip.clear { @chk_generic_proc.call(true) }
    chk_dirty_flash_tick
  end

  def chk_only_flash_my_zip_untick
    $chk_only_flash_my_zip_state = false
    @chk_only_flash_my_zip.clear { @chk_generic_proc.call(false) }
    chk_dirty_flash_untick
  end

  # toggle only_flash_my_zip by click
  def chk_only_flash_my_zip_toggle
    if $chk_only_flash_my_zip_state
      chk_only_flash_my_zip_untick
    else
      chk_only_flash_my_zip_tick
    end
  end

  $chk_skip_bootloader_unlock_state = false

  def chk_skip_bootloader_unlock_tick
    $chk_skip_bootloader_unlock_state = true
    @chk_skip_bootloader_unlock.clear { @chk_generic_proc.call(true) }
  end

  def chk_skip_bootloader_unlock_untick
    $chk_skip_bootloader_unlock_state = false
    @chk_skip_bootloader_unlock.clear { @chk_generic_proc.call(false) }
  end

  # toggle skip_bootloader_unlock by click
  def chk_skip_bootloader_unlock_toggle
    if $chk_skip_bootloader_unlock_state
      chk_skip_bootloader_unlock_untick
    else
      chk_skip_bootloader_unlock_tick
    end
  end

  $chk_reflash_twrp_state = false

  def chk_reflash_twrp_tick
    $chk_reflash_twrp_state = true
    @chk_reflash_twrp.clear { @chk_generic_proc.call(true) }
  end

  def chk_reflash_twrp_untick
    $chk_reflash_twrp_state = false
    @chk_reflash_twrp.clear { @chk_generic_proc.call(false) }
  end

  # toggle reflash_twrp by click
  def chk_reflash_twrp_toggle
    if $chk_reflash_twrp_state
      chk_reflash_twrp_untick
    else
      chk_reflash_twrp_tick
    end
  end

  $chk_use_own_twrp_state = false
  $own_twrp_path = nil

  def chk_use_own_twrp_tick
    log_on_fail do
      file_path = ask_open_file
      if file_path && file_path.end_with?(".img")
        $own_twrp_path = file_path
        $chk_use_own_twrp_state = true
        $manager.user_provided_file[:TWRP] = file_path
        @chk_use_own_twrp.clear { @chk_generic_proc.call(true) }
        $device.notifier.update_gui
      else
        $own_twrp_path = nil
      end
    end
  end

  def chk_use_own_twrp_untick
    $own_twrp_path = nil
    $chk_use_own_twrp_state = false
    $manager.user_provided_file[:TWRP] = nil
    @chk_use_own_twrp.clear { @chk_generic_proc.call(false) }
  end

  # toggle use_own_twrp by click
  def chk_use_own_twrp_toggle
    if $chk_use_own_twrp_state
      chk_use_own_twrp_untick
    else
      chk_use_own_twrp_tick
    end
  end

  $chk_dirty_flash_state = false

  def chk_dirty_flash_tick
    $chk_dirty_flash_state = true
    @chk_dirty_flash.clear { @chk_generic_proc.call(true) }
  end

  def chk_dirty_flash_untick
    $chk_dirty_flash_state = false
    @chk_dirty_flash.clear { @chk_generic_proc.call(false) }
  end

  # toggle dirty_flash by click
  def chk_dirty_flash_toggle
    if $chk_dirty_flash_state
      chk_dirty_flash_untick
    else
      chk_dirty_flash_tick
    end
  end

  $chk_sigspoof_state = true

  def chk_sigspoof_tick
    $chk_sigspoof_state = true
    @chk_sigspoof.clear { @chk_generic_proc.call(true) }
  end

  def chk_sigspoof_untick
    $chk_sigspoof_state = false
    @chk_sigspoof.clear { @chk_generic_proc.call(false) }
  end

  # toggle install signature spoofing patch by click
  def chk_sigspoof_toggle
    if $chk_sigspoof_state
      chk_sigspoof_untick
    else
      chk_sigspoof_tick
    end
  end

  $chk_swype_libs_state = false

  def chk_swype_libs_tick
    $chk_swype_libs_state = true
    @chk_swype_libs.clear { @chk_generic_proc.call(true) }
  end

  def chk_swype_libs_untick
    $chk_swype_libs_state = false
    @chk_swype_libs.clear { @chk_generic_proc.call(false) }
  end

  # toggle install swype libs by click
  def chk_swype_libs_toggle
    if $chk_swype_libs_state
      chk_swype_libs_untick
    else
      chk_swype_libs_tick
    end
  end

  $chk_gsync_adapters_state = false

  def chk_gsync_adapters_tick
    $chk_gsync_adapters_state = true
    @chk_gsync_adapters.clear { @chk_generic_proc.call(true) }
  end

  def chk_gsync_adapters_untick
    $chk_gsync_adapters_state = false
    @chk_gsync_adapters.clear { @chk_generic_proc.call(false) }
  end

  # toggle install google sync adapters by click
  def chk_gsync_adapters_toggle
    if $chk_gsync_adapters_state
      chk_gsync_adapters_untick
    else
      chk_gsync_adapters_tick
    end
  end

  @expert = flow width: 1.0, height: 0.86, margin_right: 10, margin_left: 10, margin_bottom: 10 do
    stack width: 0.5, height: 0.8, margin: 10 do
      border grey
      flow width: 1.0, margin_left: 0.12, margin_top: 24 do
        @chk_only_flash_my_zip = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_only_flash_my_zip_state)
        end
        @chk_only_flash_my_zip.click { chk_only_flash_my_zip_toggle }
        @chk_only_flash_my_zip_label = para "  only flash selected rom/zip", top: 1, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.12, margin_top: 18 do
        @chk_sigspoof = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_sigspoof_state)
        end
        @chk_sigspoof.click { chk_sigspoof_toggle }
        @chk_sigspoof_label = para "  signature spoofing patch", top: 1, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.12, margin_top: 18 do
        @chk_swype_libs = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_swype_libs_state)
        end
        @chk_swype_libs.click { chk_swype_libs_toggle }
        @chk_swype_libs_label = para "  install swype (closed source)", top: 1, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.12, margin_top: 18 do
        @chk_gsync_adapters = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_gsync_adapters_state)
        end
        @chk_gsync_adapters.click { chk_gsync_adapters_toggle }
        @chk_gsync_adapters_label = para "  install google sync adapters", top: 1, size: @fontsize
      end
    end
    stack width: 0.5, height: 0.8, margin: 10 do
      border grey
      flow width: 1.0, margin_left: 0.14, margin_top: 24 do
        @chk_skip_bootloader_unlock = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_skip_bootloader_unlock_state)
        end
        @chk_skip_bootloader_unlock.click { chk_skip_bootloader_unlock_toggle }
        @chk_skip_bootloader_unlock_label = para "  skip unlocking bootloader", top: 1, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.14, margin_top: 18 do
        @chk_reflash_twrp = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_reflash_twrp_state)
        end
        @chk_reflash_twrp.click { chk_reflash_twrp_toggle }
        @chk_reflash_twrp_label = para "  force reflash TWRP first", top: 1, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.14, margin_top: 18 do
        @chk_use_own_twrp = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_use_own_twrp_state)
        end
        @chk_use_own_twrp.click { chk_use_own_twrp_toggle }
        @chk_use_own_twrp_label = para "  use a TWRP image file", top: 1, size: @fontsize
      end
      flow width: 1.0, margin_left: 0.14, margin_top: 18 do
        @chk_dirty_flash = flow width: 20, height: 20 do
          @chk_generic_proc.call($chk_dirty_flash_state)
        end
        @chk_dirty_flash.click { chk_dirty_flash_toggle }
        @chk_dirty_flash_label = para "  do not wipe data partition", top: 1, size: @fontsize
      end
    end
    flow width: 1.0, height: 0.2, margin_bottom: 10, margin_left: 10, margin_right: 10 do
      border grey
      para "Only change these settings if you really know what you are doing.", align: "center", top: 10, size: @fontsize
    end
  end
  @expert.hide


  ### Help Panel ###

  @help = flow width: 1.0, height: 0.86, margin_right: 10, margin_left: 10, margin_bottom: 10 do
    stack width: 1.0, height: 1.0, margin: 10 do
      border grey
      flow width: 1.0, height: 65, margin_top: 0.10, margin_left: 0.25, margin_right: 0.25 do
        @btn_help = flow width: 1.0, height: 1.0 do
          inside_btn_help = flow width: 1.0, height: 1.0 do
            border mediumseagreen, strokewidth: 2
            para "Visit Homepage", width: 1.0, height: 1.0, align: "center", margin_top: 13, size: @fontsize
          end
          inside_btn_help.click { OS.open_browser("https://gitlab.com/free-droid/free-droid#help") }
        end
      end
      flow width: 1.0, height: 65, margin_top: 0.10, margin_left: 0.25, margin_right: 0.25 do
        @btn_help = flow width: 1.0, height: 1.0 do
          inside_btn_help = flow width: 1.0, height: 1.0 do
            border mediumseagreen, strokewidth: 2
            para "Try official drivers", width: 1.0, height: 1.0, align: "center", margin_top: 13, size: @fontsize
          end
          inside_btn_help.click { OS.open_browser("https://developer.android.com/studio/run/oem-usb#Drivers") }
        end
      end
      flow width: 1.0, height: 65, margin_top: 20, margin_left: 0.25, margin_right: 0.25 do
        @btn_bug_report = flow width: 1.0, height: 1.0 do
          inside_btn_help = flow width: 1.0, height: 1.0 do
            border mediumseagreen, strokewidth: 2
            para "Report a bug", width: 1.0, height: 1.0, align: "center", margin_top: 13, size: @fontsize
          end
          inside_btn_help.click {
            Log.submit
            Gui.do :alert, "The logs have been submitted. Thank you for your help."
          }
        end
      end
    end
  end
  @help.hide


  ### About Panel ###

  # On by default, but off by default in developer mode
  $chk_tracking_consent_state = !ARGV.any?{ |arg| ["dev", "-dev", "--dev", "d", "-d"].include?(arg) }

  def chk_tracking_consent_tick
    Thread.new {$reporter.report tracking: "opt-in", force: true}
    $chk_tracking_consent_state = true
    @chk_tracking_consent.clear { @chk_generic_proc.call(true, 12) }
  end

  def chk_tracking_consent_untick
    Thread.new {$reporter.report tracking: "opt-out", force: true}
    $chk_tracking_consent_state = false
    @chk_tracking_consent.clear { @chk_generic_proc.call(false, 12) }
  end

  # toggle tracking_consent by click
  def chk_tracking_consent_toggle
    if $chk_tracking_consent_state
      chk_tracking_consent_untick
    else
      chk_tracking_consent_tick
    end
  end

  @about = flow width: 1.0, height: 0.86, margin_right: 10, margin_left: 10, margin_bottom: 10 do
    flow width: 1.0, height: 1.0, margin: 10 do
      border grey
      flow width: 1.0, height: 0.9 do
        stack width: 1.0, height: 0.4 do
          @about_string = "Thanks to all the people behind these great projects:"
          para @about_string, justify: true, align: "center", margin_top: 25, size: @fontsize
        end
        flow width: 1.0, margin_left: 10, margin_right: 10, margin_top: -10 do
          @credit_links_lineageos = flow width: 0.2 do para "LineageOS", align: "center", size: @fontsize - 1 end
          @credit_links_resurrectionremix = flow width: 0.2 do para "Resurrection Remix", align: "center", size: @fontsize - 1 end
          @credit_links_omnirom = flow width: 0.2 do para "Omnirom", align: "center", size: @fontsize - 1 end
          @credit_links_aospextended = flow width: 0.2 do para "AOSP Extended", align: "center", size: @fontsize - 1 end
          @credit_links_carbonrom = flow width: 0.2 do para "CarbonROM", align: "center", size: @fontsize - 1 end
        end
        flow width: 1.0, margin_left: 10, margin_right: 10, margin_top: 5 do
          @credit_links_twrp = flow width: 0.16 do para "TWRP", align: "center", size: @fontsize - 1 end
          @credit_links_microg = flow width: 0.17 do para "MicroG", align: "center", size: @fontsize - 1 end
          @credit_links_fdroid = flow width: 0.17 do para "F-Droid", align: "center", size: @fontsize - 1 end
          @credit_links_aurora = flow width: 0.17 do para "Aurora", align: "center", size: @fontsize - 1 end
          @credit_links_magisk = flow width: 0.17 do para "Magisk", align: "center", size: @fontsize - 1 end
          @credit_links_nanodroid = flow width: 0.16 do para "NanoDroid", align: "center", size: @fontsize - 1 end
        end
        @credit_links_lineageos.click { OS.open_browser "https://lineageos.org" }
        @credit_links_twrp.click { OS.open_browser "https://twrp.me" }
        @credit_links_resurrectionremix.click { OS.open_browser "https://www.resurrectionremix.com" }
        @credit_links_omnirom.click { OS.open_browser "https://www.omnirom.org" }
        @credit_links_aospextended.click { OS.open_browser "https://aospextended.com" }
        @credit_links_carbonrom.click { OS.open_browser "https://carbonrom.org" }
        @credit_links_microg.click { OS.open_browser "https://microg.org" }
        @credit_links_fdroid.click { OS.open_browser "https://f-droid.org" }
        @credit_links_aurora.click { OS.open_browser "https://gitlab.com/AuroraOSS/AuroraStore" }
        @credit_links_magisk.click { OS.open_browser "https://forum.xda-developers.com/apps/magisk/official-magisk-v7-universal-systemless-t3473445" }
        @credit_links_nanodroid.click { OS.open_browser "https://gitlab.com/Nanolx/NanoDroid" }
        flow width: 1.0, margin_left: 0.08, margin_top: 30 do
          @chk_tracking_consent = flow width: 12, height: 12, top: 1 do
            @chk_generic_proc.call($chk_tracking_consent_state, 12)
          end
          @chk_tracking_consent.click { chk_tracking_consent_toggle }
          @chk_tracking_consent_label = para "     help improve this software by reporting device model, installation success and bugs   -  ", size: @fontsize - 3
          @view_stats_label = para "view stats", size: @fontsize - 3
          @view_stats_label.click { OS.open_browser "https://stats.free-droid.com/index.php?module=Widgetize&action=iframe&secondaryDimension=eventAction&disableLink=0&widget=1&moduleToWidgetize=Events&actionToWidgetize=getCategory&idSite=3&period=range&date=2019-07-01,today&disableLink=1&widget=1" }
        end
      end
      flow width: 1.0, height: 0.1 do
        @author_and_stuff = para "Author: Amaury Bodet  -  Version: #{VERSION}  -  Build: #{BUILD}  -  License: GPL-3.0", align: "center", size: @fontsize - 2
        @author_and_stuff.click {OS.open_browser("https://free-droid.com")}
      end
      button "test.rb", top: 28, left: 45 do
        log_on_fail(quit: false) { load "test.rb" }
      end if ARGV.any?{ |arg| ["dev", "-dev", "--dev", "d", "-d"].include?(arg) } && File.exist?("test.rb")
    end
  end
  @about.hide

  def ask_youtube_how_to_enable_android_debugging
    window title: "Help", width: 300, height: 120, resizable: false do
      background lightgrey
      para "What is the name of your device?", align: "center", width: 1.0, margin: 10, size: @fontsize
      flow width: 1.0, margin_left: 10, margin_right: 10 do @input = edit_line width: 1.0 end
      flow width: 1.0, margin: 10 do
        button "Cancel", width: 0.5, margin: 5 do close end
        button "Go", width: 0.5, margin: 5 do
          link = "https://www.youtube.com/results?search_query=#{@input.text} debugging"
          OS.open_browser link
          close
        end
      end
    end
  end

end
